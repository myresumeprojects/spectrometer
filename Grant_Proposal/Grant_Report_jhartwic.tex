\documentclass[12pt]{paper}
\title{Building a Spectrometer}
\author{Jacob Hartwick}

\usepackage{hyperref}
\usepackage{graphicx}

\begin{document}

\maketitle

\section{Introduction}

The goal of this project was to create a Czerny-Turner spectrometer for less than \$ 1000. A spectrometer is a device that is used in order to analyze the spectrum of light emitted by various sources. We already use spectrometers on campus but, these devices are black boxes, and it is hard to visualize how the device actually works. That is why we decided for this project to make it an open box, so that future students can see the optical components in order to help them understand the underlying principles. In order to create the spectrometer we decided upon these milestones:

\begin{itemize}
  \item Program Teensy in order to control the CCD array.
  \item Build Toshiba CCD Circuit.
  \item Assemble the optical components.
  \item Finish Taking Data.
  \item Finish Final Report.
\end{itemize}

The optical components are used to disperse the light determined by its wavelength, for this we followed the Czerny-Turner design as seen in Fig : \ref{fig:ct-spectrometer}. The CCD array is used to sense the light, and the Teensy microcontroller is used to control the CCD array.

\begin{figure}[ht!]
  \begin{center}
  \includegraphics[width=.9\textwidth]{./images/monochromator.png}

    \caption{This figure describes how the Czerny-Turner spectrometer works, 
    with light (A) coming in from the slit (B) to bounce off of the first concave mirror (C)
    onto the diffraction grating (D), which then reflects the light onto the second concave mirror (E),
    which is then directed towards the CCD array (F), from which we read the signal.
    \href{https://commons.wikimedia.org/wiki/File:Czerny-Turner_Monochromator.svg}{Figure (Melish, 2014)}
    }\label{fig:ct-spectrometer}
  \end{center}
\end{figure}


\section{Milestones}

\subsection{Programming the Teensy}

In the beginning we started by learning how to program the Teensy Micro-controller. We started by writing simple programs. Once we were able to read data sent from the Teensy we quickly moved to try and create the wave forms that were defined in the data sheet for the CCD array that we used, a copy can be seen in Fig : \ref{fig:timing_chart}. The actual waves produced can be seen in Fig : \ref{fig:teensy_trace}. These waves produced by the Teensy, makes the CCD array function, allowing the CCD array to detect the light, and then send that data back to the Teensy. The Teensy then would send the data to a computer, that would have software in order to produce graphs to organize the data to an easily read and understandable format for students.

\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=.9\textwidth]{./images/toshiba_timing_chart.png}
    \caption{This is the timing chart taken from the CCD array datasheet; it shows the square
    waves that need to be provided in order for the CCD array to communicate properly with
    the Teensy microcontroller.
    \href{https://toshiba.semicon-storage.com/info/TCD1304DG_Web_Datasheet_en_20190108.pdf?did=13709&prodName=TCD1304DG}{Datasheet (Toshiba, 2018)}
    }
    \label{fig:timing_chart}
  \end{center}
\end{figure}


\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/scope_trace.jpg}
  \end{center}
  \caption{The wave forms used in order to control the CCD array.}\label{fig:teensy_trace}
\end{figure}

\subsection{How to construct sensor control circuit}

Once we created the wave forms the next step was to be able to create a circuit to connect the Teensy to the CCD array in order to deliver the wave signals being produced from the Teensy.

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/full_circuit_view.jpg}
  \end{center}
  \caption{The circuit that we would use to control the CCD array.}\label{fig:ccd_array_control_circuit}
\end{figure}

\subsection{Simulation of the Czerny-Turner spectrometer w/ the Monte-Carlo method}

During this time, we were waiting for the parts to arrive, and though this was not in the original grant proposal I wanted to further understand how the Czerny-Turner spectrometer worked. This led me to create a Monte Carlo simulation of light using particles. In the simulation I programmed a light source, mirrors, and the diffraction grating. Using these optical components I was able to recreate a Czerny-Turner spectrometer within the simulation. This simulated spectrometer allowed me to move and change the components within the spectrometer to see how each component affected the system. The collimating mirror as seen in Fig : \ref{fig:collimating_mirror_screenshot}, is used to create a column of light from the source. The diffraction grating as seen in Fig : \ref{fig:diffraction_grating_screenshot}, is used in order to disperse the different wavelength of light. The diffraction grating allows us to analyze the light spectra of the various sources. The Focusing mirror as seen in Fig : \ref{fig:focusing_mirror_screenshot}, is used to take the now many, columns of light and focus them onto the CCD array, allowing it to sense the intensity of the light and this can be seen in Fig : \ref{fig:screen_screenshot}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/collumnmatting_mirror_screenshot.png}
  \end{center}
  \caption{The Collimating Mirror, collimates the light, effectively moves the light source infinitely far away from the spectrometer while maintaining a sufficient amount of light in order to analyze.}\label{fig:collimating_mirror_screenshot}
\end{figure}

Because this is created in a simulation we can adjust the size, and the angle, along with the focal length of the mirror, allowing to create different variations of the same design. This allows us to see how this mirror affects how the light behaves.

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/diffraction_grating_screenshot.png}
    The diffraction grating
  \end{center}
  \caption{The Diffraction Grating, diffracts different wavelengths of light to different angles, this separates the different wavelengths so that we can analyze each separate wavelength.}\label{fig:diffraction_grating_screenshot}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/focusing_mirror_screenshot.png}
    The Focusing Mirror
  \end{center}
  \caption{The Focusing Mirror, focuses the incoming diffracted light. This focuses each wavelength  at a different location along the screen.}\label{fig:focusing_mirror_screenshot}
\end{figure}

We can also change the different attributes of this mirror in order to see how it affects the spectrometer.

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/screen_screenshot.png}
  \end{center}
  \caption{The Screen, this is a stand in for the CCD array that would read the amount of light being focused onto the different pixels on the array, allowing us to analyze the amounts of the different wavelengths of light are present in the light source.}\label{fig:screen_screenshot}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/spectrometer_sim_screeenshot.png}
  \end{center}
  \caption{Putting together all of the previous optical components we can see the Czerny-Turner spectrometer that we are trying to build.}\label{fig:spectrometer_screenshot}
\end{figure}

\subsection{Learned how to set up optics}

Using the materials that we ordered, I was able to practice securing the mirrors, and the diffraction gratings to their mounts, and then was able to use an optical bread board to position and re-position those optics into secure locations as required. With the final locations able to be seen in \ref{fig:spectrometer}. Using this set up we were able to see the spectrum of light emitted by a LED flashlight as seen in \ref{fig:spectrum}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/spectrometer_top_light.jpg}
  \end{center}
  \caption{The Czerny-Turner Spectrometer that we where able to create.}\label{fig:spectrometer}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{./images/spectrum.jpg}
  \end{center}
  \caption{The Spectrum of light emitted from a LED flashlight given by the spectrometer that we created. }\label{fig:spectrum}
\end{figure}


\section{Reflection}

During this project I learned a lot about programming micro-controllers and embedded machinery. I was able to create a program that monitored a serial port, in order to read various information from the Teensy micro-controller that we used. This experience also allowed me to explore how different optical components interact with light. It was very interesting to see how the concave mirrors used in the spectrometer where used in two different ways. One for collimating the light, and the other for refocusing it down to a specific point. Through this time I was also able to become familiar with diffraction gratings which were something that I had never had the pleasure of working with before.

This project gave me far more experience than I ever thought it would. Being able to give a talk at MI-AAPT was a pleasant surprise that I never expected. This entire project will help me further my education and help me stand out when applying to graduate school, along with employment opportunities.

\subsection{The MI-AAPT Presentation}

The MI-AAPT presentation that I gave at Grand Valley State University was well received. In which I talked about the Monte Carlo simulation of light, and using it to create the Czerny-Turner spectrometer.
\href{https://gitlab.com/myresumeprojects/spectrometer/-/blob/trunk/MI_AAPT_Presentation/Montecarlo_Light_Simulation.pdf?ref_type=heads}{The MI AAPT presentation.}


\section{Conclusions \& Future Work}

In conclusion I was able to assemble the optics necessary for an open-box Czerny-Turner spectrometer, and visually observe the spectrum. I was able to successfully figure out how to use a Teensy Microcontroller to deliver timing signals appropriate for controlling the CCD array for measuring the spectrum. I was able to simulate the Czerny-Turner spectrometer by writing a general ray-tracing program, a result I presented at a professional physics conference.  Work remains on completing the microcontroller program, and read out the spectrum which will be carried on by future students and Dr. Nakamura.

\section{Budget Expenditure tallies}

Table of stuffs.

\section{References}
\begin{enumerate}
  \item \href{https://commons.wikimedia.org/wiki/File:Czerny-Turner_Monochromator.svg}{Figure (Melish, 2014)}
  \item \href{https://toshiba.semicon-storage.com/info/TCD1304DG_Web_Datasheet_en_20190108.pdf?did=13709&prodName=TCD1304DG}{Datasheet (Toshiba, 2018)}
  \item \url{https://www.horiba.com/int/scientific/technologies/spectrometers-and-monochromators/spectrometers-monochromators-and-spectrographs/}
  \item \url{https://www.newport.com/n/the-grating-equation}
  \item \url{https://www.newport.com/mam/celum/celum_assets/np/resources/MKS_Diffraction_Grating_Handbook.pdf}
\end{enumerate}


\end{document}
