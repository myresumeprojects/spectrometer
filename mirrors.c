#include "Include/mirrors.h"
#include "Include/matrix.h"
#include "Include/particles.h"
#include "Include/rays.h"
#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>

void InitMirrors(surface_t *plane_mirrors, int surface_num,
                 concave_mirror_t *concave_mirrors, int concave_mirror_num) {
  InitConcaveMirrors(concave_mirrors, concave_mirror_num);
  InitPlaneSurfaces(plane_mirrors);
}

void InitConcaveMirrors(concave_mirror_t *concave_mirrors,
                        int concave_mirror_num) {
  // Initialize the sub mirrors of the concave mirror, around the center of
  // curvature
  for (int i = 0; i < concave_mirror_num; i++) {
    concave_mirrors[i].mirrors.surface_max = SUB_MIRRORS;
    InitSurfaces(&concave_mirrors[i].mirrors);
    for (int j = 0; j < SUB_MIRRORS; j++) {
      // Starting X
      concave_mirrors[i].mirrors.start[j].M[0] =
          concave_mirrors[i].radius *
              cos(-concave_mirrors[i].offset * PI +
                  j / (double)SUB_MIRRORS * concave_mirrors[i].size * PI) +
          concave_mirrors[i].pos.M[0];
      // Starting Y
      concave_mirrors[i].mirrors.start[j].M[1] =
          concave_mirrors[i].radius *
              sin(-concave_mirrors[i].offset * PI +
                  j / (double)SUB_MIRRORS * concave_mirrors[i].size * PI) +
          concave_mirrors[i].pos.M[1];
      // Ending X
      concave_mirrors[i].mirrors.end[j].M[0] =
          concave_mirrors[i].radius * cos(-concave_mirrors[i].offset * PI +
                                          (j + 1) / (double)SUB_MIRRORS *
                                              concave_mirrors[i].size * PI) +
          concave_mirrors[i].pos.M[0];
      // Ending Y
      concave_mirrors[i].mirrors.end[j].M[1] =
          concave_mirrors[i].radius * sin(-concave_mirrors[i].offset * PI +
                                          (j + 1) / (double)SUB_MIRRORS *
                                              concave_mirrors[i].size * PI) +
          concave_mirrors[i].pos.M[1];
      concave_mirrors[i].mirrors.surface_num++;
    }
  }
  // Calculate the normal for all the sub mirrors

  for (int i = 0; i < concave_mirror_num; i++) {
    for (int j = 0; j < concave_mirrors[i].mirrors.surface_num; j++) {
      matrix_2x1_t normal = concave_mirrors[i].mirrors.normal[j];
      normal = CalculateNormalMatrix(concave_mirrors[i].mirrors.start[j],
                                     concave_mirrors[i].mirrors.end[j], normal);
      concave_mirrors[i].mirrors.normal[j] = normal;
      concave_mirrors[i].mirrors.surface_type[j] = MIRROR;
      concave_mirrors[i].mirrors.reflectance[j] =
          concave_mirrors[i].reflectance;
      concave_mirrors[i].mirrors.color[j] = BLUE;
      concave_mirrors[i].mirrors.CollideParticle[j] = &CollideParticleMirror;
      concave_mirrors[i].mirrors.CollideRay[j] = &CollideRayMirror;
    }
    int leftMirrorX = 2147483647;
    int rightMirrorX = -2147483647;
    int topMirrorY = 2147483647;
    int bottomMirrorY = -2147483647;

    for (int j = 0; j < SUB_MIRRORS; j++) {
      if (concave_mirrors[i].mirrors.start[j].M[0] < leftMirrorX) {
        leftMirrorX = concave_mirrors[i].mirrors.start[j].M[0];
      }
      if (concave_mirrors[i].mirrors.start[j].M[0] > rightMirrorX) {
        rightMirrorX = concave_mirrors[i].mirrors.start[j].M[0];
      }
      if (concave_mirrors[i].mirrors.start[j].M[1] < topMirrorY) {
        topMirrorY = concave_mirrors[i].mirrors.start[j].M[1];
      }
      if (concave_mirrors[i].mirrors.start[j].M[1] > bottomMirrorY) {
        bottomMirrorY = concave_mirrors[i].mirrors.start[j].M[1];
      }
    }
    concave_mirrors[i].bounding_Rect = (Rectangle){
        leftMirrorX - 10, topMirrorY - 10, rightMirrorX - leftMirrorX + 20,
        bottomMirrorY - topMirrorY + 20

    };
  }
}

void InitSurfaces(surface_t *surfaces) {
  surfaces->surface_num = 0;
  surfaces->surface_type = (int *)calloc(surfaces->surface_max, sizeof(int));
  surfaces->start =
      (matrix_2x1_t *)calloc(surfaces->surface_max, sizeof(matrix_2x1_t));
  surfaces->end =
      (matrix_2x1_t *)calloc(surfaces->surface_max, sizeof(matrix_2x1_t));
  surfaces->normal =
      (matrix_2x1_t *)calloc(surfaces->surface_max, sizeof(matrix_2x1_t));
  surfaces->color = (Color *)calloc(surfaces->surface_max, sizeof(Color));
  surfaces->reflectance =
      (double *)calloc(surfaces->surface_max, sizeof(double));
  surfaces->CollideParticle =
      (void (**)(particle_t *, Color *, matrix_2x1_t))calloc(
          surfaces->surface_max,
          sizeof(void (*)(particle_t *, Color *, matrix_2x1_t)));
  surfaces->CollideRay = (void (**)(ray_t *, matrix_2x1_t, matrix_2x1_t))calloc(
      surfaces->surface_max,
      sizeof(void (*)(particle_t *, matrix_2x1_t, matrix_2x1_t)));
}

void CalculateSurfaceNormals(surface_t *surfaces) {
  for (int i = 0; i < surfaces->surface_num; i++) {
    surfaces->normal[i] = CalculateNormalMatrix(
        surfaces->start[i], surfaces->end[i], surfaces->normal[i]);
  }
}

void InitPlaneSurfaces(surface_t *surfaces) {
  for (int i = 0; i < surfaces->surface_num; i++) {
    matrix_2x1_t normal = surfaces->normal[i];
    normal =
        CalculateNormalMatrix(surfaces->start[i], surfaces->end[i], normal);
    surfaces->normal[i] = normal;
    surfaces->reflectance[i] = surfaces->reflectance[i];
    if (surfaces->surface_type[i] == MIRROR) {
      surfaces->color[i] = RAYWHITE;
      surfaces->CollideParticle[i] = &CollideParticleMirror;
      surfaces->CollideRay[i] = &CollideRayMirror;
    }
    if (surfaces->surface_type[i] == WALL) {
      surfaces->color[i] = BROWN;
      surfaces->CollideParticle[i] = &CollideParticleWall;
      surfaces->CollideRay[i] = &CollideRayWall;
    }
    if (surfaces->surface_type[i] == GRADIENT) {
      surfaces->color[i] = GREEN;
      surfaces->CollideParticle[i] = &CollideParticleGradient;
      surfaces->CollideRay[i] = &CollideRayGradient;
    }
  }
}

void InitLenses(lens_t *lenses) {
  lenses->lenses_num = 0;
  lenses->pos =
      (matrix_2x1_t *)calloc(sizeof(matrix_2x1_t), lenses->lenses_max);
  lenses->width = (double *)calloc(sizeof(double), lenses->lenses_max);
  lenses->height = (double *)calloc(sizeof(double), lenses->lenses_max);
  lenses->left_radius = (double *)calloc(sizeof(double), lenses->lenses_max);
  lenses->right_radius = (double *)calloc(sizeof(double), lenses->lenses_max);

  for (int i = 0; i < lenses->lenses_max; i++) {
    lenses->surfaces[i].surface_max = 202;
    InitSurfaces(&lenses->surfaces[i]);
  }
}

void AddConcaveMirrors(concave_mirror_t *concave_mirrors,
                       int *concave_mirror_num, int *concave_mirror_max,
                       matrix_2x1_t pos, double radius, double size,
                       double offset) {
  // Add a concave mirror to the scene
  if (*concave_mirror_num < *concave_mirror_max) {
    concave_mirrors[*concave_mirror_num].pos = pos;
    concave_mirrors[*concave_mirror_num].radius = radius;
    concave_mirrors[*concave_mirror_num].size = size;
    concave_mirrors[*concave_mirror_num].offset = offset;
    concave_mirrors[*concave_mirror_num].reflectance = 1.0f;
    *concave_mirror_num = *concave_mirror_num + 1;
  } else {
    printf("Not Enough Room to add another Concave Mirror\n");
  }
}

void AddPlaneMirror(surface_t *plane_mirrors, matrix_2x1_t start,
                    matrix_2x1_t end, double reflectance) {
  // add a plane mirror to the scene
  if (plane_mirrors->surface_num < plane_mirrors->surface_max) {
    plane_mirrors->surface_type[plane_mirrors->surface_num] = MIRROR;
    plane_mirrors->start[plane_mirrors->surface_num] = start;
    plane_mirrors->end[plane_mirrors->surface_num] = end;
    plane_mirrors->reflectance[plane_mirrors->surface_num] = reflectance;
    plane_mirrors->surface_num++;
  } else {
    printf("Not Enough Room to add another Plane Mirror\n");
  }
}
void AddWall(surface_t *surface, matrix_2x1_t start, matrix_2x1_t end,
             double reflectance) {
  // add a plane mirror to the scene
  if (surface->surface_num < surface->surface_max) {
    surface->surface_type[surface->surface_num] = WALL;
    surface->start[surface->surface_num] = start;
    surface->end[surface->surface_num] = end;
    surface->reflectance[surface->surface_num] = reflectance;
    surface->surface_num++;
  } else {
    printf("Not Enough Room to add another Wall\n");
  }
}

void AddGradient(surface_t *surface, matrix_2x1_t start, matrix_2x1_t end,
                 double reflectance) {
  // add a plane mirror to the scene
  if (surface->surface_num < surface->surface_max) {
    surface->surface_type[surface->surface_num] = GRADIENT;
    surface->start[surface->surface_num] = start;
    surface->end[surface->surface_num] = end;
    surface->reflectance[surface->surface_num] = reflectance;
    surface->surface_num++;
  } else {
    printf("Not Enough Room to add another Gradient\n");
  }
}

void AddLens(lens_t *lenses, matrix_2x1_t pos, double width, double height,
             double left_radius, double right_radius) {

  lenses->pos[lenses->lenses_num] = pos;
  lenses->width[lenses->lenses_num] = width;
  lenses->height[lenses->lenses_num] = height;
  lenses->left_radius[lenses->lenses_num] = left_radius;
  lenses->right_radius[lenses->lenses_num] = right_radius;
  for (int i = 0; i < lenses->surfaces[lenses->lenses_num].surface_max / 2 - 1;
       i++) {
    lenses->surfaces[lenses->lenses_num].start[i] = (matrix_2x1_t){
        lenses->pos[lenses->lenses_num].M[0] -
            lenses->width[lenses->lenses_num] / 2 +
            lenses->left_radius[lenses->lenses_num] *
                cos((double)(2 * i) /
                    (lenses->surfaces[lenses->lenses_num].surface_max - 2)),
        lenses->pos[lenses->lenses_num].M[0] -
            lenses->height[lenses->lenses_num] / 2 +
            lenses->left_radius[lenses->lenses_num] *
                sin((double)(2 * i) /
                    (lenses->surfaces[lenses->lenses_num].surface_max - 2))};
  }
  for (int i = 0; i < lenses->surfaces[lenses->lenses_num].surface_max / 2 - 1;
       i++) {
    lenses->surfaces[lenses->lenses_num]
        .start[i + lenses->surfaces[lenses->lenses_num].surface_max / 2 - 1] =
        (matrix_2x1_t){
            lenses->pos[lenses->lenses_num].M[0] -
                lenses->width[lenses->lenses_num] / 2 +
                lenses->left_radius[lenses->lenses_num] *
                    cos((double)(2 * i) /
                        (lenses->surfaces[lenses->lenses_num].surface_max - 2)),
            lenses->pos[lenses->lenses_num].M[0] -
                lenses->height[lenses->lenses_num] / 2 +
                lenses->left_radius[lenses->lenses_num] *
                    sin((double)(2 * i) /
                        (lenses->surfaces[lenses->lenses_num].surface_max -
                         2))};
  }

  for (int i = 0; i < lenses->surfaces[lenses->lenses_num].surface_max; i++) {
    lenses->surfaces[lenses->lenses_num].color[i] = RAYWHITE;
  }
  lenses->lenses_num++;
}

void DrawSurfaces(surface_t *surfaces) {
  // For each plane mirror
  for (int i = 0; i < surfaces->surface_num; i++) {
    // Draw a line to represent the mirror
    DrawLine(surfaces->start[i].M[0], surfaces->start[i].M[1],
             surfaces->end[i].M[0], surfaces->end[i].M[1], surfaces->color[i]);
    // Draw the normal of the mirror
    int startX = (surfaces->end[i].M[0] - surfaces->start[i].M[0]) / 2 +
                 surfaces->start[i].M[0];
    int startY = (surfaces->end[i].M[1] - surfaces->start[i].M[1]) / 2 +
                 surfaces->start[i].M[1];
    int endX = (surfaces->end[i].M[0] - surfaces->start[i].M[0]) / 2 +
               surfaces->normal[i].M[0] * 20 + surfaces->start[i].M[0];
    int endY = (surfaces->end[i].M[1] - surfaces->start[i].M[1]) / 2 +
               surfaces->normal[i].M[1] * 20 + surfaces->start[i].M[1];

    DrawLine(startX, startY, endX, endY, RED);
  }
}

void DrawConcaveMirrors(concave_mirror_t *concave_mirrors,
                        int concave_mirror_num) {
  // For each concave mirror
  for (int i = 0; i < concave_mirror_num; i++) {
    // DrawRectangleLinesEx(concave_mirrors[i].bounding_Rect, 5, RED);
    //  Draw a circle to represent the center of curvature
    DrawCircle(concave_mirrors[i].pos.M[0], concave_mirrors[i].pos.M[1], 10,
               GREEN);
    // Draw a line that represents the focal point
    DrawCircle(
        concave_mirrors[i].pos.M[0] + concave_mirrors[i].radius / 2 *
                                          cos(-concave_mirrors[i].offset * PI +
                                              PI / 2 * concave_mirrors[i].size),
        concave_mirrors[i].pos.M[1] + concave_mirrors[i].radius / 2 *
                                          sin(-concave_mirrors[i].offset * PI +
                                              PI / 2 * concave_mirrors[i].size),
        5, GREEN);

    // For each sub mirror in a mirror
    for (int j = 0; j < SUB_MIRRORS; j++) {

      // Draw the line that represents the mirror
      DrawLine(concave_mirrors[i].mirrors.start[j].M[0],
               concave_mirrors[i].mirrors.start[j].M[1],
               concave_mirrors[i].mirrors.end[j].M[0],
               concave_mirrors[i].mirrors.end[j].M[1], BLUE);
      // Draw the normal to the submirror
      int startX = (concave_mirrors[i].mirrors.end[j].M[0] -
                    concave_mirrors[i].mirrors.start[j].M[0]) /
                       2 +
                   concave_mirrors[i].mirrors.start[j].M[0];
      int startY = (concave_mirrors[i].mirrors.end[j].M[1] -
                    concave_mirrors[i].mirrors.start[j].M[1]) /
                       2 +
                   concave_mirrors[i].mirrors.start[j].M[1];
      int endX =
          (concave_mirrors[i].mirrors.end[j].M[0] -
           concave_mirrors[i].mirrors.start[j].M[0]) /
              2 +
          concave_mirrors[i].mirrors.normal[j].M[0] * MIRROR_NORMAL_LENGTH +
          concave_mirrors[i].mirrors.start[j].M[0];
      int endY =
          (concave_mirrors[i].mirrors.end[j].M[1] -
           concave_mirrors[i].mirrors.start[j].M[1]) /
              2 +
          concave_mirrors[i].mirrors.normal[j].M[1] * MIRROR_NORMAL_LENGTH +
          concave_mirrors[i].mirrors.start[j].M[1];

      DrawLine(startX, startY, endX, endY, RED);
    }
  }
}

void DrawMirrors(surface_t *surfaces, concave_mirror_t *concave_mirrors,
                 bool draw_plane_mirrors, bool draw_concave_mirrors,
                 int surface_num, int concave_mirror_num) {
  if (draw_plane_mirrors) {
    DrawSurfaces(surfaces);
  }

  if (draw_concave_mirrors) {
    DrawConcaveMirrors(concave_mirrors, concave_mirror_num);
  }
}

void DrawLenses(lens_t *lenses) {
  for (int i = 0; i < lenses->lenses_num; i++) {
    DrawSurfaces(lenses[i].surfaces);
  }
}
