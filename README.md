# Spectrometer

## Purpose

The purpose of this project is to build a working Czernzy-Turner Spectrometer from scratch.
This project uses a CCD array, 2 concave mirrors, a diffraction grating, and a pinhole in order to make a Spectrometer.
A Light simulation was also made during this experience.

## Theory

### Mirrors

Plane Mirrors Observe the law of reflectance.

$$ \theta_i = \theta_r $$
Let
$$
\hat{x} = cos^{-1}(\theta_i) \\
\hat{y} = sin^{-1}(\theta_r)
$$
$$
\hat{r} = \begin{bmatrix}
\hat{x} \\ \hat{y} \\
\end{bmatrix}
$$

Where $\hat{r}$ is the direction of the ray

$$
M_m =
\begin{bmatrix}
1 - 2 \hat{n_x}^2 & -2\hat{n_x}\hat{n_y} \\
-2\hat{n_x}\hat{n_y} & 1 - 2 \hat{n_y}^2 \\
\end{bmatrix}
$$
Where M_M is the matrix associated with a Mirror M. 
Where $\hat{n_x}$ is the x component of normal of the mirror M that the ray (or particle) is colliding with, and $\hat{n_y}$ is the y component of the mirror M that the ray (or particle) is colliding with.

$$
\hat{r}' = M_M \hat{r}
$$

Where $\hat{r}'$ is the direction of the outgoing ray.

### Diffraction Grating
The Grating Equation
$$ d * (sin \theta_i - sin \theta_r) = m * \lambda $$

Rearranged Grating Equation to find $\beta$
$$
\beta (\lambda) = sin^{-1}(sin\alpha - \frac{m\lambda}{d})
$$


$$
\hat{r} = \begin{bmatrix}
\hat{x} \\ \hat{y} \\
\end{bmatrix}
$$

Let $M_G$ be the matrix associated with a Diffraction Grating G.

$$
M_G = 
\begin{bmatrix}
1 - 2cos(\beta)^2 & -2cos(\beta) sin(\beta) \\
-2cos(\beta)sin(\beta) & 1 - 2sin(\beta)^2 \\
\end{bmatrix}
$$


$$
\hat{r}' = M_G \hat{r}
$$

### Czernzy-Turner


## Procedure

We originally started with a He-Ne laser shining on a grating, bouncing into a camera, in order to find the proper angle to set the camera at.

### Teensy Setup

First We went to here [Teensy USB Development Board](https://www.pjrc.com/teensy/) in order to figure out the software development tools required for programming the teensy. After reading about the various ways We both decided that the [Teensyduino](https://www.pjrc.com/teensy/teensyduino.html) route would be best, or rather easiest to get working quickly.

#### Arch Setup
As I am using the Arch Linux operating system it might be useful for me to describe what I did to set the process up. In order to get the arduino IDE on my laptop I found the AUR package [arduino-ide-bin](https://aur.archlinux.org/packages/arduino-ide-bin) made sure the package was up to date with the regular arduino IDE, then downloaded it using yay (which is an easy way of getting packages from the aur). Then continued to follow the [Tutorial Steps](https://www.pjrc.com/teensy/tutorial.html) on the Teensy website. Including the Linux only part where you install the UDEV rules. (very important step).

### Tutorials
In order to figure out how to program the Teensy we followed the first Blink tutorial, along with the RGB LED Tutorial, except for the fact, we didn't have an RGB LED, so we used 3 different LEDs, of 3 different colors. Insert Diagram here ???? (I did take a picture on my phone, so this should be re-creatable).

### Programming the Teensy 

Originally we used the `analogWrite()` to program the teensy in order to drive the original circuit was able to make the LED's light up in the Tutorials, along with the initial attempts to make the waves in order to drive the CCD array.
When we removed the LED's from circuit, we used the oscilliscope to monitor wave forms. Using `analogWrite()` we noticed significant waveform drift.
Error is the % Error in the period from what was programmed (10ms or microseconds) and the period measured,
Testing revealed about 7.2% error in millisecond waveforms with 1ms on and 9ms off.
Error jumped to over 7000% in microsecond waveforms with 1 microsecond on and 9 microsecond off.

After switching to the `digitalWrite()` function in the code the Error was resolved down to the nanosecond scale.

## Data


## Analysis

## Conclusion

## Links and References Used

### Hardware Information

- [ESP 32 datasheet](https://www.espressif.com/sites/default/files/documentation/esp32-pico_series_datasheet_en.pdf) We Did Not Go with this

- [Toshiba CCD Array](https://www.digikey.com/en/products/detail/toshiba-semiconductor-and-storage/TCD1304DG-8Z-K/16572684) We Went With this

- [Toshiba CCD Array Datasheet](https://toshiba.semicon-storage.com/info/TCD1304DG_Web_Datasheet_en_20190108.pdf?did=13709&prodName=TCD1304DG) We Went With this

- [Hamamatsu CCD Array S11639-01](https://www.hamamatsu.com/us/en/product/optical-sensors/image-sensor/ccd-cmos-nmos-image-sensor/line-sensor/for-spectrophotometry/S11639-01.html) Did not go with this for monetary reasons

- [Hamamatsu CCD Array S11639-11](https://www.hamamatsu.com/us/en/product/optical-sensors/image-sensor/ccd-cmos-nmos-image-sensor/line-sensor/for-spectrophotometry/S11639-11.html) Did not go with this for monetary reasons

- [Teensy Micro Controller](https://www.pjrc.com/teensy/) We Went With This

- [Teensy Micro Controller We Used](https://www.pjrc.com/store/teensy41.html)

### Theory References

- [Spectrometers and Monochromators](https://www.horiba.com/int/scientific/technologies/spectrometers-and-monochromators/spectrometers-monochromators-and-spectrographs/)
- [The Grating Equation](https://www.newport.com/n/the-grating-equation)
- [Diffraction Grating Handbook](https://www.newport.com/mam/celum/celum_assets/np/resources/MKS_Diffraction_Grating_Handbook.pdf)
