#include "Include/manager.h"
#include "Include/matrix.h"
#include "Include/mirrors.h"
#include "Include/particles.h"
#include "Include/rays.h"
#include <raylib.h>
#include <stdio.h>

void InitManager(manager *m) {

  // For Changing the View
  m->camera = (Camera2D){0};
  m->camera.zoom = 1.0f;
  printf("Init Camera\n");

  // For Being able to move the mirrors and gratings and Walls
  m->selectedConcaveMirrorIdx = -1;
  m->selectedSourceIdx = -1;
  m->selectedPlaneIdx = -1;
  m->selectedPlaneStartIdx = -1;
  m->selectedPlaneEndIdx = -1;
  m->simArea =
      (Rectangle){0, 60, GetScreenWidth() - 300, GetScreenHeight() - 60};

  // The number of rays
  m->ray_max = 10000;
  m->ray_num = 5000;
  m->rays = (ray_t *)calloc(m->ray_max, sizeof(ray_t));
  m->GENERATE_RAYS = true;
  m->drawAllRays = true;
  m->use_rays = false;
  m->last_ray_update = 0;
  m->ray_update_delay = 0.05;
  printf("Init Rays\n");

  // The Number of maximum particles
  m->particle_max = 10000;
  // The current number of particles in the sim
  m->particle_num = 8000;
  // allocate memory
  m->particles = (particle_t *)calloc(m->particle_max, sizeof(particle_t));
  m->particle_colors = (Color *)calloc(m->particle_max, sizeof(Color));
  // The speed at which the particles travel
  m->particle_speed = 200.0f;
  m->use_particles = false;
  printf("Init Particles\n");

  // Area that if the particles go out of then, they get reinitialized
  m->kill_box = (Rectangle){-1000, -1000, 10000, 10000};
  printf("Init Bounding Box\n");
  // For the Walls

  m->source_max = 10;
  m->source_num = 0;
  m->sources = (source_t *)calloc(m->source_max, sizeof(matrix_2x1_t));

  // Add a source to the scene
  AddSource(m->sources, &m->source_num, &m->source_max,
            (matrix_2x1_t){{-900, 0}}, true);
  printf("Init sources\n");

  m->concave_mirror_max = 10;
  m->concave_mirror_num = 0;
  m->concave_mirrors = (concave_mirror_t *)calloc(m->concave_mirror_max,
                                                  sizeof(concave_mirror_t));
  // Add The Top Mirror
  AddConcaveMirrors(m->concave_mirrors, &m->concave_mirror_num,
                    &m->concave_mirror_max,
                    (matrix_2x1_t){m->sources[0].pos.M[0] - 6000, 1000}, 12000,
                    3 / 128.0f, 5 / 128.0f);

  // Add the bottom Mirror
  AddConcaveMirrors(m->concave_mirrors, &m->concave_mirror_num,
                    &m->concave_mirror_max, (matrix_2x1_t){-1000, 2800}, 6000,
                    1 / 16.0f, 3 / 32.0f);

  printf("Init Concave Mirrors\n");
  m->plane_mirror_num = 1;
  m->wall_num = 6;
  m->gradient_num = 1;
  m->plane_surfaces.surface_max = 100;
  InitSurfaces(&m->plane_surfaces);

  // For adding the box around the point source, in order to make a slit source
  matrix_2x1_t topleft = (matrix_2x1_t){-910, -10};
  matrix_2x1_t topright = (matrix_2x1_t){-850, -10};
  matrix_2x1_t bottomleft = (matrix_2x1_t){-910, 10};
  matrix_2x1_t bottomright = (matrix_2x1_t){-850, 10};

  AddPlaneMirror(&m->plane_surfaces, (matrix_2x1_t){-950, 100},
                 (matrix_2x1_t){-950, -100}, 1.0f);
  AddWall(&m->plane_surfaces, (matrix_2x1_t){topleft.M[0] - 10, topleft.M[1]},
          (matrix_2x1_t){topright.M[0] + 10, topright.M[1]}, 1.0f);
  AddWall(&m->plane_surfaces,
          (matrix_2x1_t){bottomright.M[0] + 10, bottomright.M[1]},
          (matrix_2x1_t){bottomleft.M[0] - 10, bottomleft.M[1]}, 1.0f);

  AddWall(&m->plane_surfaces,
          (matrix_2x1_t){bottomleft.M[0], bottomleft.M[1] + 10},
          (matrix_2x1_t){topleft.M[0], topleft.M[1] - 10}, 1.0f);

  AddWall(&m->plane_surfaces, (matrix_2x1_t){topright.M[0], topright.M[1] - 10},
          (matrix_2x1_t){topright.M[0], topright.M[1] + 7}, 1.0f);

  AddWall(&m->plane_surfaces,
          (matrix_2x1_t){bottomright.M[0], bottomright.M[1] - 7},
          (matrix_2x1_t){bottomright.M[0], bottomright.M[1] + 10}, 1.0f);

  AddWall(&m->plane_surfaces, (matrix_2x1_t){2300, 3200},
          (matrix_2x1_t){2300, 2800}, 1.0f);
  // Add the gradient to the scene
  AddGradient(&m->plane_surfaces, (matrix_2x1_t){-975, 1500},
              (matrix_2x1_t){-975, 500}, 1.0f);
  InitPlaneSurfaces(&m->plane_surfaces);

  InitConcaveMirrors(m->concave_mirrors, m->concave_mirror_num);

  if (m->use_particles) {
    for (int i = 0; i < m->particle_num; i++) {
      InitParticle(m->particles + i, m->particle_colors + i,
                   m->sources + rand() % m->source_num);
    }
  }
  if (m->use_rays) {
    for (int i = 0; i < m->ray_num; i++) {
      InitRay(m->rays + i, m->sources);
    }
    m->GENERATE_RAYS = false;
  }
}

void UpdateSelected(manager *m, Vector2 mousePos) {
  // Move Sources

  bool mouseInSimArea = CheckCollisionPointRec(GetMousePosition(), m->simArea);
  if (mouseInSimArea) {
    if (m->mousePressed) {
      m->selectedSourceIdx = -1;
      m->selectedPlaneIdx = -1;
      m->selectedConcaveMirrorIdx = -1;
      m->selectedPlaneStartIdx = -1;
      m->selectedPlaneEndIdx = -1;
    }

    for (int i = 0; i < m->source_num; i++) {
      bool collision = CheckCollisionPointCircle(
          mousePos, (Vector2){m->sources[i].pos.M[0], m->sources[i].pos.M[1]},
          10);

      if (collision && m->mousePressed) {
        m->selectedSourceIdx = i;
      }
    }

    bool collision = CheckCollisionPointCircle(
        mousePos,
        (Vector2){m->sources[m->selectedSourceIdx].pos.M[0],
                  m->sources[m->selectedSourceIdx].pos.M[1]},
        10);
    if (m->selectedSourceIdx != -1 && IsMouseButtonDown(0) && collision) {
      m->moveSelected = true;
    }

    if (m->selectedSourceIdx != -1 && m->moveSelected && m->mouseReleased) {
      m->moveSelected = false;
      m->sources[m->selectedSourceIdx].pos.M[0] = mousePos.x;
      m->sources[m->selectedSourceIdx].pos.M[1] = mousePos.y;

      m->GENERATE_RAYS = true;
    }

    // Move Plane Surface
    for (int i = 0; i < m->plane_surfaces.surface_num; i++) {
      // Select A Plane Surface
      if (CheckCollisionPointCircle(mousePos,
                                    (Vector2){m->plane_surfaces.start[i].M[0],
                                              m->plane_surfaces.start[i].M[1]},
                                    10) &&
          m->mousePressed) {
        m->selectedPlaneIdx = i;
      }
      if (CheckCollisionPointCircle(mousePos,
                                    (Vector2){m->plane_surfaces.end[i].M[0],
                                              m->plane_surfaces.end[i].M[1]},
                                    10) &&
          m->mousePressed) {
        m->selectedPlaneIdx = i;
      }
    }
    if (m->selectedPlaneIdx != -1) {
      bool collisionStart = CheckCollisionPointCircle(
          mousePos,
          (Vector2){m->plane_surfaces.start[m->selectedPlaneIdx].M[0],
                    m->plane_surfaces.start[m->selectedPlaneIdx].M[1]},
          10);
      bool collisionEnd = CheckCollisionPointCircle(
          mousePos,
          (Vector2){m->plane_surfaces.end[m->selectedPlaneIdx].M[0],
                    m->plane_surfaces.end[m->selectedPlaneIdx].M[1]},
          10);
      // Set down start of mirror
      if (IsMouseButtonDown(0) && collisionStart) {
        m->moveSelected = true;
        m->selectedPlaneStartIdx = m->selectedPlaneIdx;
      }
      if (IsMouseButtonDown(0) && collisionEnd) {
        m->moveSelected = true;
        m->selectedPlaneEndIdx = m->selectedPlaneIdx;
      }

      if (m->moveSelected && m->mouseReleased &&
          m->selectedPlaneStartIdx != -1) {
        m->plane_surfaces.start[m->selectedPlaneStartIdx].M[0] = mousePos.x;
        m->plane_surfaces.start[m->selectedPlaneStartIdx].M[1] = mousePos.y;

        CalculateSurfaceNormals(&m->plane_surfaces);
        m->GENERATE_RAYS = true;
      }
      // set down end of mirror
      if (m->moveSelected && m->mouseReleased && m->selectedPlaneEndIdx != -1) {
        m->plane_surfaces.end[m->selectedPlaneEndIdx].M[0] = mousePos.x;
        m->plane_surfaces.end[m->selectedPlaneEndIdx].M[1] = mousePos.y;

        CalculateSurfaceNormals(&m->plane_surfaces);
        m->GENERATE_RAYS = true;
      }
    }
    // Move Concave mirrors

    for (int i = 0; i < m->concave_mirror_num; i++) {
      if ((CheckCollisionPointCircle(mousePos,
                                     (Vector2){m->concave_mirrors[i].pos.M[0],
                                               m->concave_mirrors[i].pos.M[1]},
                                     10)) &&
          m->mousePressed) {
        m->selectedConcaveMirrorIdx = i;
      }
    }

    if (m->selectedConcaveMirrorIdx) {
      bool collideConcave = CheckCollisionPointCircle(
          mousePos,
          (Vector2){m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[0],
                    m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[1]},
          10);
      if (IsMouseButtonDown(0) && collideConcave) {
        m->moveSelected = true;
      }

      if (m->moveSelected && m->mouseReleased &&
          m->selectedConcaveMirrorIdx != -1) {
        m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[0] = mousePos.x;
        m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[1] = mousePos.y;

        InitConcaveMirrors(m->concave_mirrors, m->concave_mirror_num);
        m->GENERATE_RAYS = true;
        m->moveSelected = false;
      }
    }
  }
}

void UpdateManager(manager *m, float deltaTime, Vector2 mousePos) {

  // Update Particles if using particles
  if (m->use_particles) {
    // Spawn more particles if W is pressed
    if (IsKeyDown(KEY_W)) {
      if (m->particle_num < m->particle_max - 10) {
        for (int i = 0; i < 10; i++) {
          m->particle_num++;
          InitParticle(m->particles + m->particle_num,
                       m->particle_colors + m->particle_num,
                       m->sources + rand() % m->source_num);
        }
      }
    }
    // Reduce the amount of particles if S is pressed
    if (IsKeyDown(KEY_S)) {
      if (m->particle_num > 10) {
        m->particle_num -= 10;
      }
    }

    for (int i = 0; i < m->particle_num; i++) {
      for (int j = 0; j < m->plane_surfaces.surface_num; j++) {
        matrix_2x1_t collision_point;
        if (CheckCollisionMatrix(
                m->particles[i].position,
                Add2x1by2x1(m->particles[i].position,
                            Scale2x1(m->particles[i].velocity, deltaTime)),
                m->plane_surfaces.start[j], m->plane_surfaces.end[j],
                &collision_point)) {
          float x = rand() % 1000;
          if (x / 1000 <= m->plane_surfaces.reflectance[j]) {
            m->plane_surfaces.CollideParticle[j](&m->particles[i],
                                                 &m->particle_colors[i],
                                                 m->plane_surfaces.normal[j]);
          }
        }
      }

      for (int j = 0; j < m->concave_mirror_num; j++) {
        if (CheckCollisionPointRec((Vector2){m->particles[i].position.M[0],
                                             m->particles[i].position.M[1]},
                                   m->concave_mirrors[j].bounding_Rect)) {
          for (int k = 0; k < m->concave_mirrors[j].mirrors.surface_num; k++) {
            matrix_2x1_t collision_point;
            if (CheckCollisionMatrix(
                    m->particles[i].position,
                    Add2x1by2x1(m->particles[i].position,
                                Scale2x1(m->particles[i].velocity, deltaTime)),
                    m->concave_mirrors[j].mirrors.start[k],
                    m->concave_mirrors[j].mirrors.end[k], &collision_point)) {

              float x = rand() % 1000;
              if (x / 1000 <= m->concave_mirrors[j].mirrors.reflectance[k]) {
                m->concave_mirrors[j].mirrors.CollideParticle[k](
                    &m->particles[i], &m->particle_colors[i],
                    m->concave_mirrors[j].mirrors.normal[k]);
              }
            }
          }
        }
      }

      UpdateParticle(m->particles + i, deltaTime);
      // If Parlticle out of bounds, reset particle
      if (m->particles[i].position.M[0] > m->kill_box.x + m->kill_box.width ||
          m->particles[i].position.M[0] < m->kill_box.x ||
          m->particles[i].position.M[1] > m->kill_box.y + m->kill_box.height ||
          m->particles[i].position.M[1] < m->kill_box.y) {
        m->particles[i].active = false;
      }

      if (!m->particles[i].active) {
        InitParticle(m->particles + i, m->particle_colors + i,
                     m->sources + rand() % m->source_num);
      }
    }
  }

  // Update Rays if using rays
  if (m->use_rays) {
    if (m->GENERATE_RAYS) {
      for (int i = 0; i < m->ray_num; i++) {
        InitRay(m->rays + i, m->sources + rand() % m->source_num);
      }
      m->GENERATE_RAYS = false;
    }

    if (m->last_ray_update + m->ray_update_delay < GetTime()) {
      m->last_ray_update = GetTime();
      for (int i = 0; i < m->ray_num; i++) {
        if (m->rays[i].collisions[m->rays[i].collision_num].M[0] >
                m->kill_box.x + m->kill_box.width ||
            m->rays[i].collisions[m->rays[i].collision_num].M[0] <
                m->kill_box.x ||
            m->rays[i].collisions[m->rays[i].collision_num].M[1] >
                m->kill_box.y + m->kill_box.height ||
            m->rays[i].collisions[m->rays[i].collision_num].M[1] <
                m->kill_box.y) {
          m->rays[i].active = false;
        }

        bool hit = false;
        bool repeat = false;

        if (m->rays[i].active) {
          for (int j = 0; j < m->plane_surfaces.surface_num; j++) {
            matrix_2x1_t collision_point;
            if (CheckCollisionMatrix(
                    m->rays[i].collisions[m->rays[i].collision_num],
                    Add2x1by2x1(
                        m->rays[i].collisions[m->rays[i].collision_num],
                        Scale2x1(
                            m->rays[i].collision_dir[m->rays[i].collision_num],
                            RAY_COLLISION_DISTANCE)),
                    m->plane_surfaces.start[j], m->plane_surfaces.end[j],
                    &collision_point)) {
              if ((float)(rand() % 1000) / 1000 <=
                  m->plane_surfaces.reflectance[j]) {
                m->plane_surfaces.CollideRay[j](&m->rays[i], collision_point,
                                                m->plane_surfaces.normal[j]);
                hit = true;
              }
            }
          }
          for (int j = 0; j < m->concave_mirror_num; j++) {
            for (int k = 0; k < m->concave_mirrors->mirrors.surface_num; k++) {
              matrix_2x1_t collision_point;
              if (CheckCollisionMatrix(
                      m->rays[i].collisions[m->rays[i].collision_num],
                      Add2x1by2x1(
                          m->rays[i].collisions[m->rays[i].collision_num],
                          Scale2x1(m->rays[i]
                                       .collision_dir[m->rays[i].collision_num],
                                   RAY_COLLISION_DISTANCE)),
                      m->concave_mirrors[j].mirrors.start[k],
                      m->concave_mirrors[j].mirrors.end[k], &collision_point)) {
                if ((float)(rand() % 1000) / 1000 <=
                    m->concave_mirrors[j].mirrors.reflectance[k]) {
                  m->concave_mirrors[j].mirrors.CollideRay[k](
                      &m->rays[i], collision_point,
                      m->concave_mirrors[j].mirrors.normal[k]);
                  hit = true;
                }
              }
            }
          }

          if (hit) {
            m->rays[i].collision_num++;
          }
          UpdateRay(m->rays + i, deltaTime);
        }
      }
    }
  }

  UpdateSelected(m, mousePos);
}

void DrawSelected(manager *m, Vector2 mousePos) {
  // TODO: REDO The select System
  //
  // Draw the circle for where the point source will be
  if (m->moveSelected && m->selectedSourceIdx != -1) {
    DrawCircle(mousePos.x, mousePos.y, 10, WHITE);
  }
  // Draw the line to where the plane mirror will be
  if (m->selectedPlaneIdx != -1) {
    DrawCircle(m->plane_surfaces.start[m->selectedPlaneIdx].M[0],
               m->plane_surfaces.start[m->selectedPlaneIdx].M[1], 10, RED);
    DrawCircle(m->plane_surfaces.end[m->selectedPlaneIdx].M[0],
               m->plane_surfaces.end[m->selectedPlaneIdx].M[1], 10, RED);
    if (m->mouseDown && !m->mousePressed && m->selectedPlaneStartIdx != -1) {
      DrawLine(mousePos.x, mousePos.y,
               m->plane_surfaces.end[m->selectedPlaneStartIdx].M[0],
               m->plane_surfaces.end[m->selectedPlaneStartIdx].M[1],
               m->plane_surfaces.color[m->selectedPlaneStartIdx]);
    }
    if (m->mouseDown && !m->mousePressed && m->selectedPlaneEndIdx != -1) {
      DrawLine(m->plane_surfaces.start[m->selectedPlaneEndIdx].M[0],
               m->plane_surfaces.start[m->selectedPlaneEndIdx].M[1], mousePos.x,
               mousePos.y, m->plane_surfaces.color[m->selectedPlaneEndIdx]);
    }
  }

  // Draw where the center of curvature is
  // Draw a line that represents where the mirror will be placed
  if (m->moveSelected && m->selectedConcaveMirrorIdx != -1) {
    DrawCircle(mousePos.x, mousePos.y, 10, GREEN);
    DrawLine(mousePos.x +
                 m->concave_mirrors[m->selectedConcaveMirrorIdx]
                     .mirrors.start[0]
                     .M[0] -
                 m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[0],
             mousePos.y +
                 m->concave_mirrors[m->selectedConcaveMirrorIdx]
                     .mirrors.start[0]
                     .M[1] -
                 m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[1],
             mousePos.x +
                 m->concave_mirrors[m->selectedConcaveMirrorIdx]
                     .mirrors.end[SUB_MIRRORS - 1]
                     .M[0] -
                 m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[0],
             mousePos.y +
                 m->concave_mirrors[m->selectedConcaveMirrorIdx]
                     .mirrors.end[SUB_MIRRORS - 1]
                     .M[1] -
                 m->concave_mirrors[m->selectedConcaveMirrorIdx].pos.M[1],
             BLUE);
  }
}
