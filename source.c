#include "Include/source.h"
#include "stdio.h"

void AddSource(source_t *sources, int *source_num, int *source_max,
               matrix_2x1_t pos, bool is_point_source) {
  // Add a source to the scene
  if (*source_num < *source_max) {
    sources[*source_num].pos = pos;
    sources[*source_num].point_source = is_point_source;
    *source_num = *source_num + 1;
  } else {
    printf("Not Enough room to add another Source\n");
  }
}
void DrawSources(source_t *sources, int num_sources) {
  // Draw Source Icons
  for (int i = 0; i < num_sources; i++) {
    DrawCircle(sources[i].pos.M[0], sources[i].pos.M[1], 10, WHITE);
  }
}
