#include "./Include/rays.h"
#include "Include/matrix.h"
#include "stdio.h"
#include <raylib.h>

void InitRay(ray_t *ray, source_t *source) {
  // Might Overhall rays
  // TODO : Comment Rays
  ray->collision_num = 0;
  ray->interesting_collisions_num = 0;
  ray->active = true;
  ray->wave_length = -1;
  float opacity = .5;
  Color ray_color = (Color){255, 255, 150, 255};

  for (int i = 0; i < RAY_COLLISION_NUM; i++) {
    ray->collisions[i] = (matrix_2x1_t){{100, 100}};
    ray->collision_dir[i] = (matrix_2x1_t){{0, 0}};
    ray->distances[i] = RAY_COLLISION_DISTANCE;
    ray->colors[i] = Fade(ray_color, opacity);
  }
  if (source->point_source) {
    ray->position.M[0] = source->pos.M[0];
    ray->position.M[1] = source->pos.M[1];
    ray->direction.M[0] = (rand() % 1000 - 500);
    ray->direction.M[1] = (rand() % 1000 - 500);
    ray->direction = NormalizeMatrix(ray->direction);
  } else {
    ray->position.M[0] = source->pos.M[0];
    ray->position.M[1] = source->pos.M[1] + (rand() % 500 - 250);
    ray->direction.M[0] = 1.0l;
    ray->direction.M[1] = 0.0l;
    ray->direction = NormalizeMatrix(ray->direction);
  }
  ray->collisions[0].M[0] = ray->position.M[0];
  ray->collisions[0].M[1] = ray->position.M[1];

  ray->collision_dir[0].M[0] = ray->direction.M[0];
  ray->collision_dir[0].M[1] = ray->direction.M[1];
}

// Check Collisions
void CollideRayMirror(ray_t *ray, matrix_2x1_t collision_point,
                      matrix_2x1_t normal) {
  double dist = MatrixMagnitude(
      Sub2x1by2x1(collision_point, ray->collisions[ray->collision_num]));

  if (ray->distances[ray->collision_num] > dist) {
    ray->distances[ray->collision_num] = dist;
    ray->collisions[ray->collision_num + 1] = collision_point;
    matrix_2x1_t modified_matrix =
        multiply_2x2_by_2x1(calc_plane_mirror_matrix(normal),
                            ray->collision_dir[ray->collision_num]);
    modified_matrix = NormalizeMatrix(modified_matrix);

    ray->collision_dir[ray->collision_num + 1] = modified_matrix;
    ray->interesting_collisions_num++;
  }
}
void CollideRayWall(ray_t *ray, matrix_2x1_t collision_point,
                    matrix_2x1_t normal) {
  double dist = MatrixMagnitude(
      Sub2x1by2x1(collision_point, ray->collisions[ray->collision_num]));
  if (ray->distances[ray->collision_num] > dist) {
    ray->distances[ray->collision_num] = dist;
    ray->active = false;
    ray->collisions[ray->collision_num + 1] = collision_point;
    ray->collision_dir[ray->collision_num + 1] =
        ray->collision_dir[ray->collision_num];
  }
}
void CollideRayGradient(ray_t *ray, matrix_2x1_t collision_point,
                        matrix_2x1_t normal) {
  double dist = MatrixMagnitude(
      Sub2x1by2x1(collision_point, ray->collisions[ray->collision_num]));
  if (ray->distances[ray->collision_num] > dist) {
    ray->distances[ray->collision_num] = dist;
    ray->collisions[ray->collision_num + 1] = collision_point;
    ray->collision_dir[ray->collision_num + 1] =
        ray->collision_dir[ray->collision_num];
    // use the gradient equation to adjust the normal
    float beta = 0;
    int mode = 1;
    // if the particle is white light
    if (ray->wave_length < 0) {
      // assign random wave_length to the particle
      int x = rand() % 4;
      if (x == 0) {
        ray->wave_length = 500 * pow(10, -9);
      } else if (x == 1) {
        ray->wave_length = 700 * pow(10, -9);
      } else if (x == 2) {
        ray->wave_length = 600 * pow(10, -9);
      } else if (x == 3) {
        ray->wave_length = 400 * pow(10, -9);
      }
    }
    float opacity = 0.5;
    // opacity is a float that should be between 0 and 1
    if (ray->wave_length == 500 * pow(10, -9)) {
      for (int i = ray->collision_num + 1; i < RAY_COLLISION_NUM; i++) {
        ray->colors[i] = Fade(BLUE, opacity);
      }
    } else if (ray->wave_length == 700 * pow(10, -9)) {
      for (int i = ray->collision_num + 1; i < RAY_COLLISION_NUM; i++) {
        ray->colors[i] = Fade(RED, opacity);
      }
    } else if (ray->wave_length == 600 * pow(10, -9)) {
      for (int i = ray->collision_num + 1; i < RAY_COLLISION_NUM; i++) {
        ray->colors[i] = Fade(GREEN, opacity);
      }
    } else if (ray->wave_length == 400 * pow(10, -9)) {
      for (int i = ray->collision_num + 1; i < RAY_COLLISION_NUM; i++) {
        ray->colors[i] = Fade(VIOLET, opacity);
      }
    }

    // use wave_length to apply gradient equation
    beta = asin(normal.M[1] - mode * ray->wave_length * 60000);
    normal.M[0] = cos(beta);
    normal.M[1] = sin(beta);

    NormalizeMatrix(normal);
    // Apply Mirror Matrix with new normal
    matrix_2x1_t mv =
        multiply_2x2_by_2x1(calc_plane_mirror_matrix(normal),
                            ray->collision_dir[ray->collision_num]);

    ray->collision_dir[ray->collision_num + 1] = mv;
    ray->interesting_collisions_num++;
  }
}

void UpdateRay(ray_t *ray, double deltaTime) {
  if (ray->active) {
    // Propogate Ray
    ray->collisions[ray->collision_num + 1] =
        Add2x1by2x1(ray->collisions[ray->collision_num],
                    Scale2x1(ray->collision_dir[ray->collision_num],
                             RAY_COLLISION_DISTANCE));
    ray->collision_dir[ray->collision_num + 1].M[0] =
        ray->collision_dir[ray->collision_num].M[0];
    ray->collision_dir[ray->collision_num + 1].M[1] =
        ray->collision_dir[ray->collision_num].M[1];

    ray->collision_num++;
    if (ray->collision_num >= RAY_COLLISION_NUM) {
      ray->active = false;
    }
  }
}

void DrawLightRay(ray_t *ray, bool drawAllRays) {
  // Ray Initial Conditions
  if (false) {
    DrawLine(ray->position.M[0], ray->position.M[1],
             ray->position.M[0] + ray->direction.M[0] * 1000,
             ray->position.M[1] + ray->direction.M[1] * 1000, PURPLE);
  }
  // Actual Ray
  if (ray->interesting_collisions_num > 0 || drawAllRays) {
    for (int j = 0; j < ray->collision_num; j++) {
      DrawLineEx(
          (Vector2){ray->collisions[j].M[0], ray->collisions[j].M[1]},
          (Vector2){ray->collisions[j + 1].M[0], ray->collisions[j + 1].M[1]},
          5, ray->colors[j]);
    }
  }
  // Last Part Of The Ray
  if (ray->collision_num > 0 && false) {
    DrawLine(ray->collisions[ray->collision_num].M[0],
             ray->collisions[ray->collision_num].M[1],
             ray->collisions[ray->collision_num].M[0] +
                 ray->collision_dir[ray->collision_num].M[0] *
                     RAY_COLLISION_DISTANCE,
             ray->collisions[ray->collision_num].M[1] +
                 ray->collision_dir[ray->collision_num].M[1] *
                     RAY_COLLISION_DISTANCE,
             ray->colors[ray->collision_num]);
  }
}
