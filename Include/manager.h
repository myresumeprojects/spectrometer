#ifndef MANAGER_H
#define MANAGER_H

#include "mirrors.h"
#include "particles.h"
#include "rays.h"
#include "source.h"
#include <raylib.h>

typedef struct manager manager;

struct manager {
  int source_max;
  int source_num;
  source_t *sources;

  int plane_mirror_num;
  int wall_num;
  int gradient_num;
  surface_t plane_surfaces;

  int concave_mirror_max;
  int concave_mirror_num;
  concave_mirror_t *concave_mirrors;

  int particle_num;
  int particle_max;
  particle_t *particles;
  Color *particle_colors;
  float particle_speed;
  bool use_particles;

  int ray_num;
  int ray_max;
  bool GENERATE_RAYS;
  bool drawAllRays;
  ray_t *rays;
  bool use_rays;
  double last_ray_update;
  double ray_update_delay;

  Rectangle simArea;
  bool mousePressed;
  bool mouseDown;
  bool mouseReleased;
  bool moveSelected;
  int selectedConcaveMirrorIdx;
  int selectedPlaneIdx;
  int selectedSourceIdx;
  int selectedPlaneStartIdx;
  int selectedPlaneEndIdx;

  Rectangle kill_box;

  Camera2D camera;
};

void InitManager(manager *m);
void UpdateManager(manager *m, float deltaTime, Vector2 mousePos);
void DrawSelected(manager *m, Vector2 mousePos);

#endif
