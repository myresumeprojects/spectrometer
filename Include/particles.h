#ifndef PARTICLE_H
#define PARTICLE_H

#include "matrix.h"
#include "source.h"
#include <raylib.h>

typedef struct particle particle_t;

struct particle {
  bool active;
  matrix_2x1_t position;
  matrix_2x1_t velocity;
  double wave_length;
};

void InitParticle(particle_t *particle, Color *particle_color,
                  source_t *source);

void CollideParticleMirror(particle_t *particle, Color *particle_color,
                           matrix_2x1_t normal_matrix);
void CollideParticleWall(particle_t *particle, Color *particle_color,
                         matrix_2x1_t normal_matrix);
void CollideParticleGradient(particle_t *particle, Color *particle_color,
                             matrix_2x1_t normal_matrix);
bool CollideParticle(particle_t *particle, float deltaTime, matrix_2x1_t start,
                     matrix_2x1_t end);

void UpdateParticle(particle_t *particle, double deltaTime);

void DrawParticle(particle_t *particle, Texture2D particle_texture,
                  Color *particle_color, int i);
#endif
