#ifndef MIRRORS
#define MIRRORS

#include "matrix.h"
#include "particles.h"
#include "rays.h"
#include <raylib.h>

#define USE_POINT_SOURCE true
#define MIRROR_NORMAL_LENGTH 20

#define SUB_MIRRORS 200

typedef struct surface surface_t;
typedef struct concave_mirror concave_mirror_t;
typedef struct lens lens_t;

enum surface_type { MIRROR, WALL, GRADIENT };

struct surface {
  int surface_num;
  int surface_max;
  int *surface_type;
  matrix_2x1_t *start;
  matrix_2x1_t *end;
  matrix_2x1_t *normal;
  double *reflectance;
  Color *color;
  void (**CollideParticle)(particle_t *particle, Color *particle_color,
                           matrix_2x1_t normal);
  void (**CollideRay)(ray_t *ray, matrix_2x1_t collision_point,
                      matrix_2x1_t normal);
};

struct concave_mirror {
  matrix_2x1_t pos;
  Rectangle bounding_Rect;
  double radius;
  double size;
  double offset;
  double reflectance;
  surface_t mirrors;
};

struct lens {
  int lenses_max;
  int lenses_num;
  matrix_2x1_t *pos;
  double *width;
  double *height;
  double *left_radius;
  double *right_radius;
  surface_t *surfaces;
};

void InitSurfaces(surface_t *surfaces);

void InitConcaveMirrors(concave_mirror_t *concave_mirrors,
                        int concave_mirror_num);

void InitLenses(lens_t *lenses);

void CalculateSurfaceNormals(surface_t *surfaces);
void InitPlaneSurfaces(surface_t *surfaces);

void AddConcaveMirrors(concave_mirror_t *concave_mirrors,
                       int *concave_mirror_num, int *concave_mirror_max,
                       matrix_2x1_t pos, double radius, double size,
                       double offset);
void AddPlaneMirror(surface_t *plane_mirrors, matrix_2x1_t start,
                    matrix_2x1_t end, double reflectance);
void AddWall(surface_t *surfaces, matrix_2x1_t start, matrix_2x1_t end,
             double reflectance);
void AddGradient(surface_t *surfaces, matrix_2x1_t start, matrix_2x1_t end,
                 double grooves);

void AddLens(lens_t *lenses, matrix_2x1_t pos, double width, double height,
             double left_radius, double right_radius);

void DrawMirrors(surface_t *surfaces, concave_mirror_t *concave_mirrors,
                 bool draw_plane_mirrors, bool draw_concave_mirrors,
                 int surface_num, int concave_mirror_num);
#endif
