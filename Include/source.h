#ifndef SOURCE_H
#define SOURCE_H
#include "matrix.h"

typedef struct source source_t;

struct source {
  matrix_2x1_t pos;
  bool point_source;
};

void AddSource(source_t *sources, int *num_sources, int *source_max,
               matrix_2x1_t pos, bool is_point_source);
void DrawSources(source_t *sources, int num_sources);

#endif // !SOURCE_H
