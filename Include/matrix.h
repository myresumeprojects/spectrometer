#ifndef MATRIX
#define MATRIX

#include <math.h>
#include <raylib.h>
#include <stdlib.h>

typedef struct {
  double M[4];
} matrix_2x2_t;

typedef struct {
  double M[2];
} matrix_2x1_t;

matrix_2x2_t calc_mirror_matrix(matrix_2x1_t normal);
matrix_2x2_t calc_plane_mirror_matrix(matrix_2x1_t normal);
matrix_2x2_t calc_concave_mirror_matrix(matrix_2x1_t normal);
matrix_2x1_t multiply_2x2_by_2x1(matrix_2x2_t m1, matrix_2x1_t m2);

matrix_2x1_t cramer(matrix_2x2_t m1, matrix_2x1_t m2);
double det(matrix_2x2_t m);

matrix_2x1_t Scale2x1(matrix_2x1_t m, double scalar);
matrix_2x1_t Add2x1by2x1(matrix_2x1_t m1, matrix_2x1_t m2);
matrix_2x1_t Sub2x1by2x1(matrix_2x1_t m1, matrix_2x1_t m2);

bool CheckCollisionMatrix(matrix_2x1_t startPos1, matrix_2x1_t endPos1,
                          matrix_2x1_t startPos2, matrix_2x1_t endPos2,
                          matrix_2x1_t *collisionPoint);
double MatrixDistance(matrix_2x1_t m1, matrix_2x1_t m2);
double MatrixMagnitude(matrix_2x1_t m);

matrix_2x1_t NormalizeMatrix(matrix_2x1_t m);
matrix_2x1_t CalculateNormalMatrix(matrix_2x1_t m1, matrix_2x1_t m2,
                                   matrix_2x1_t normal);
double Magnitude(double *vector);
void Normalize(double *vector);
double VectorDotProduct(double *vector1, double *vector2);
void CalculateNormal(double *vector1, double *vector2, double *normal);
#endif // !MATRIX
