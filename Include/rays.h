#ifndef RAY_H
#define RAY_H

#include "matrix.h"

#include "source.h"

#define RAY_COLLISION_NUM 100
#define RAY_COLLISION_DISTANCE 500

typedef struct ray ray_t;
struct ray {
  matrix_2x1_t position;
  matrix_2x1_t direction;
  int collision_num;
  int interesting_collisions_num;
  matrix_2x1_t collisions[RAY_COLLISION_NUM];
  matrix_2x1_t collision_dir[RAY_COLLISION_NUM];
  double distances[RAY_COLLISION_NUM];
  Color colors[RAY_COLLISION_NUM];
  double wave_length;
  bool active;
};

void InitRay(ray_t *ray, source_t *source);

void CollideRayMirror(ray_t *ray, matrix_2x1_t collision_point,
                      matrix_2x1_t normal);
void CollideRayWall(ray_t *ray, matrix_2x1_t collision_point,
                    matrix_2x1_t normal);
void CollideRayGradient(ray_t *ray, matrix_2x1_t collision_point,
                        matrix_2x1_t normal);
void CollideRaysConcaveMirrors(ray_t *ray, int i);
void CollideRays(ray_t ray, int i);

void UpdateRay(ray_t *rays, double deltaTime);
void DrawLightRay(ray_t *ray, bool drawAllRays);

#endif
