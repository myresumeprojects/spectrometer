#include "./Include/matrix.h"

#include <math.h>
#include <raylib.h>

matrix_2x2_t calc_mirror_matrix(matrix_2x1_t normal) {
  return (matrix_2x2_t){
      {1 - 2 * normal.M[0] * normal.M[0], -2 * normal.M[0] * normal.M[1],
       -2 * normal.M[0] * normal.M[1], 1 - 2 * normal.M[1] * normal.M[1]}};
}
matrix_2x2_t calc_plane_mirror_matrix(matrix_2x1_t normal) {
  return (matrix_2x2_t){{1.0 - 2.0 * normal.M[0] * normal.M[0],
                         -2.0 * normal.M[0] * normal.M[1],
                         -2.0 * normal.M[0] * normal.M[1],
                         1.0 - 2.0 * normal.M[1] * normal.M[1]}};
}
matrix_2x2_t calc_concave_mirror_matrix(matrix_2x1_t normal) {
  return (matrix_2x2_t){
      {1 - 2 * normal.M[0] * normal.M[0], -2 * normal.M[0] * normal.M[1],
       -2 * normal.M[0] * normal.M[1], 1 - 2 * normal.M[1] * normal.M[1]}};
}

matrix_2x1_t Scale2x1(matrix_2x1_t m, double scalar) {
  return (matrix_2x1_t){m.M[0] * scalar, m.M[1] * scalar};
}

matrix_2x1_t Add2x1by2x1(matrix_2x1_t m1, matrix_2x1_t m2) {
  return (matrix_2x1_t){m1.M[0] + m2.M[0], m1.M[1] + m2.M[1]};
}
matrix_2x1_t Sub2x1by2x1(matrix_2x1_t m1, matrix_2x1_t m2) {
  return (matrix_2x1_t){m1.M[0] - m2.M[0], m1.M[1] - m2.M[1]};
}

matrix_2x1_t multiply_2x2_by_2x1(matrix_2x2_t m1, matrix_2x1_t m2) {
  matrix_2x1_t result = (matrix_2x1_t){{m1.M[0] * m2.M[0] + m1.M[2] * m2.M[1],
                                        m1.M[1] * m2.M[0] + m1.M[3] * m2.M[1]}};
  return result;
}

matrix_2x1_t cramer(matrix_2x2_t m1, matrix_2x1_t m2) {
  matrix_2x1_t result =
      (matrix_2x1_t){{(m2.M[0] * m1.M[3] - m1.M[1] * m2.M[1]) /
                          (m1.M[0] * m1.M[3] - m1.M[1] * m1.M[2]),
                      (m2.M[1] * m1.M[0] - m1.M[1] * m2.M[0]) /
                          (m1.M[0] * m1.M[3] - m1.M[1] * m1.M[2])}};
  return result;
}

double det(matrix_2x2_t m) { return (m.M[0] * m.M[3]) - (m.M[0] * m.M[1]); }

bool CheckCollisionMatrix(matrix_2x1_t startPos1, matrix_2x1_t endPos1,
                          matrix_2x1_t startPos2, matrix_2x1_t endPos2,
                          matrix_2x1_t *collisionPoint) {
  bool collision = false;

  double div =
      (endPos2.M[1] - startPos2.M[1]) * (endPos1.M[0] - startPos1.M[0]) -
      (endPos2.M[0] - startPos2.M[0]) * (endPos1.M[1] - startPos1.M[1]);

  if (fabs(div) >= 0) {
    collision = true;

    double xi =
        ((startPos2.M[0] - endPos2.M[0]) *
             (startPos1.M[0] * endPos1.M[1] - startPos1.M[1] * endPos1.M[0]) -
         (startPos1.M[0] - endPos1.M[0]) *
             (startPos2.M[0] * endPos2.M[1] - startPos2.M[1] * endPos2.M[0])) /
        div;
    double yi =
        ((startPos2.M[1] - endPos2.M[1]) *
             (startPos1.M[0] * endPos1.M[1] - startPos1.M[1] * endPos1.M[0]) -
         (startPos1.M[1] - endPos1.M[1]) *
             (startPos2.M[0] * endPos2.M[1] - startPos2.M[1] * endPos2.M[0])) /
        div;

    if (((fabs(startPos1.M[0] - endPos1.M[0]) > 0) &&
         (xi < fmin(startPos1.M[0], endPos1.M[0]) ||
          (xi > fmax(startPos1.M[0], endPos1.M[0])))) ||
        ((fabs(startPos2.M[0] - endPos2.M[0]) > 0) &&
         (xi < fmin(startPos2.M[0], endPos2.M[0]) ||
          (xi > fmax(startPos2.M[0], endPos2.M[0])))) ||
        ((fabs(startPos1.M[1] - endPos1.M[1]) > 0) &&
         (yi < fmin(startPos1.M[1], endPos1.M[1]) ||
          (yi > fmax(startPos1.M[1], endPos1.M[1])))) ||
        ((fabs(startPos2.M[1] - endPos2.M[1]) > 0) &&
         (yi < fmin(startPos2.M[1], endPos2.M[1]) ||
          (yi > fmax(startPos2.M[1], endPos2.M[1])))))
      collision = false;

    if (collision && (collisionPoint != 0)) {
      collisionPoint->M[0] = xi;
      collisionPoint->M[1] = yi;
    }
  }

  return collision;
}

double MatrixDistance(matrix_2x1_t m1, matrix_2x1_t m2) {
  matrix_2x1_t m = (matrix_2x1_t){{m2.M[0] - m1.M[0], m2.M[1] - m1.M[1]}};
  return MatrixMagnitude(m);
}
double MatrixMagnitude(matrix_2x1_t m) {
  return sqrt(m.M[0] * m.M[0] + m.M[1] * m.M[1]);
}

matrix_2x1_t NormalizeMatrix(matrix_2x1_t m) {
  double magnitude = MatrixMagnitude(m);

  m.M[0] = m.M[0] / magnitude;
  m.M[1] = m.M[1] / magnitude;
  return m;
}

matrix_2x1_t CalculateNormalMatrix(matrix_2x1_t m1, matrix_2x1_t m2,
                                   matrix_2x1_t normal) {
  matrix_2x1_t m = {{m2.M[0] - m1.M[0], m2.M[1] - m1.M[1]}};
  normal.M[0] = -m.M[1];
  normal.M[1] = m.M[0];
  return NormalizeMatrix(normal);
}
double Magnitude(double *vector) {
  return sqrt(vector[0] * vector[0] + vector[1] * vector[1]);
}

double VectorDotProduct(double *vector1, double *vector2) {
  return vector1[0] * vector2[0] + vector1[1] * vector2[1];
}
void CalculateNormal(double *vector1, double *vector2, double *normal) {
  double vector[2] = {vector2[0] - vector1[0], vector2[1] - vector1[1]};
  matrix_2x2_t trans;
  trans.M[0] = vector[0] * vector[0] - vector[1] * vector[1];
  trans.M[1] = 2 * vector[0] * vector[1];
  trans.M[2] = 2 * vector[0] * vector[1];
  trans.M[3] = vector[1] * vector[1] - vector[0] * vector[0];
  normal[0] = trans.M[0] * vector[0] + trans.M[1] * vector[1];
  normal[1] = trans.M[2] * vector[0] + trans.M[3] * vector[1];
}
