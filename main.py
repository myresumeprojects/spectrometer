import sys
import serial
import pygame

SERIAL_PORT = "/dev/ttyACM0"
BAUD_RATE = 38400

ser = serial.Serial(SERIAL_PORT, BAUD_RATE)

time_vals = []
x_vals = []
y_vals = []


def read_data():
    line = ser.readline().decode("utf-8").strip()
    vals = line.split(", ")
    return (vals[0], vals[1], vals[2])


def on_close():
    with open("data.csv", "w") as file:
        file.write("TIME, X, Y\n")
        for i, x in enumerate(x_vals):
            file.write(f"{time_vals[i]}, {x_vals[i]}, {y_vals[i]}\n")


SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720


def main():
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    running = True
    frame_counter = 0
    update_frame = False
    while running:
        frame_counter += 1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        vals = read_data()
        time_vals.append(float(vals[0]))
        x_vals.append(float(vals[1]))
        y_vals.append(float(vals[2]))

        if frame_counter > 60:
            frame_counter = 0
            update_frame = True

        if update_frame:
            update_frame = False
            x_min = x_vals[0]
            x_max = x_vals[-1]
            y_min = 0
            y_max = 255

            for i, x in enumerate(x_vals):
                y = y_vals[i]
                if x < x_min:
                    x_min = x
                if x > x_max:
                    x_max = x

                if y < y_min:
                    y_min = y
                if y > y_max:
                    y_max = y

            x_scale = 0
            if x_min == x_max:
                x_scale = 1
            else:
                x_scale = (x_max - x_min) / (SCREEN_WIDTH - 200)

                screen.fill("white")
                for i, x in enumerate(x_vals):
                    y = y_vals[i]
                    pygame.draw.circle(
                        screen,
                        "black",
                        (
                            (x - x_min) / (x_scale) + 100,
                            -y / y_max * 300 + SCREEN_HEIGHT - 100,
                        ),
                        10,
                    )
                pygame.display.update()

    on_close()
    pygame.quit()


if __name__ == "__main__":
    main()
