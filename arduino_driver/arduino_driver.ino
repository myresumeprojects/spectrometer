uint32_t oldLow = ARM_DWT_CYCCNT;
uint32_t curHigh = 0;


int out_1_pin = 18; // 
int out_2_pin = 15; // 
int out_3_pin = 14;

int human_time_scale = 1;

// These timings are from the CCD array datasheet.
int t1 = 5000 * human_time_scale;
int t2 = 500 * human_time_scale;
int t3 = 1500 * human_time_scale;
int t4 = 20 * human_time_scale;
int t_int = 1000000;
int counter = 0;
int clock_signal = LOW;
int ICG_signal = LOW;
int SH_signal = LOW;


unsigned long start_time;
unsigned long seconds;
unsigned long minutes;
unsigned long hours;
unsigned long days;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("This Definitly Works \n");
  Serial.println(F_CPU_ACTUAL);
  Serial.println(ARM_DWT_CYCCNT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(out_1_pin, OUTPUT);
  pinMode(out_2_pin, OUTPUT);
  pinMode(out_3_pin, OUTPUT);
  start_time = micros();
  analogWriteFrequency(out_1_pin, 2000000);
  analogWrite(out_1_pin, 128);
}

void loop() {
  // put your main code here, to run repeatedly:

  //output(out_1_pin, out_2_pin, out_3_pin, t1, t2, t3, 80, t_int);
  ccd_timing(out_2_pin, out_3_pin);
}

uint64_t getCnt(){
  uint32_t curLow = ARM_DWT_CYCCNT;
  if(curLow < oldLow){
    curHigh++;
  }
  oldLow=curLow;
  uint64_t curVal = ((uint64_t)curHigh << 32) | curLow;

  return curVal;
}


void output(int channel_1, int channel_2, int channel_3, int t1, int t2, int t3, int t4, int t_int){
  // function that drives the timing of the ccd array
  delayNanoseconds(250);
  counter++;

  if (counter % 2 == 0){
    digitalWrite(channel_1, HIGH);
  } else {
    digitalWrite(channel_1, LOW);
  }

  if (counter % 28 == 0){ \\ because (t1 + t2 + t3) / 250 = 28
  \\ So every 28 counts start a new cycle
  \\ do t2 stuff and finish the previous cycle
    digitalWrite(channel_2, LOW);
    digitalWrite(channel_3, HIGH);

  }
  if (counter % 28 == 2){
    \\ do t3 stuff (look at datasheet)

  }
  if(counter % 28 == 8){
    \\ do t1 stuff
  }

  
}

void random_numbers(){
    for (int i = 0; i < 2048; i++){
    Serial.print(micros());
    Serial.print(", ");
    Serial.print(i);
    Serial.print(", ");
    Serial.println(random(1, 255));
    delay(1);
  }
}


// This shows the 2 waves for driving the ccd array
// this function does not however incorporate the clock wave
void ccd_timing(int channel_1, int channel_2){
  digitalWrite(channel_2, LOW);
  delayNanoseconds(t2);
  digitalWrite(channel_1, HIGH);
  delayNanoseconds(t3);
  digitalWrite(channel_1, LOW);
  delayNanoseconds(t1);
  digitalWrite(channel_2, HIGH);
  delayNanoseconds(t_int);
}

// This function was used on 11-20-2024 and it created a 50MHz pulse wave in  theory
// Using an oscilliscope we saw that the pulse width was nearly 4 times. (At 25ns the pulse was 194ns).
void quick(int channel_1){
  clock_signal = !clock_signal;
  digitalWrite(channel_1, clock_signal);
  delayNanoseconds(20);
}

// This function can be used to send short pulses, with 2 channels in order to test 
// the delay between 2 channels how short the delay can be
void test(int channel_1, int channel_2, int test_delay_on, int test_delay_off){
  // test_delay_on is the delay for how long the channels will be on
  // test_delay_off is the delay for how long the channels will be off
  digitalWrite(out_2_pin, HIGH);
  digitalWrite(out_1_pin, HIGH);
  delayNanoseconds(test_delay_on);
  digitalWrite(out_2_pin, LOW);
  digitalWrite(out_1_pin, LOW);
  delayNanoseconds(test_delay_off);
}


//  ANALOG BAD!!!
void analog_testing(int channel_1, int channel_2, int test_delay_on, int test_delay_off){
  analogWrite(channel_1, HIGH);
  analogWrite(channel_2, HIGH);
  delayMicroseconds(test_delay_on);
  analogWrite(channel_1, LOW);
  analogWrite(channel_2, LOW);
  delayMicroseconds(test_delay_off);
}
