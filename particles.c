#include "./Include/manager.h"
#include <raylib.h>

void InitParticle(particle_t *particle, Color *particle_color,
                  source_t *source) {
  float particle_speed = 200.0f;
  particle->active = true;
  // Initialize a particle
  // Choose a point source
  int start_pos_idx = 0;
  if (source->point_source) {
    // Initialize a particle at a point source
    particle->position.M[0] = source->pos.M[0];
    particle->position.M[1] = source->pos.M[1];
    // Set random velocity
    particle->velocity.M[0] = (rand() % 1000 - 500);
    particle->velocity.M[1] = (rand() % 1000 - 500);
    particle->velocity = NormalizeMatrix(particle->velocity);
    particle->velocity.M[0] *= particle_speed;
    particle->velocity.M[1] *= particle_speed;

  } else {
    // Initialize a particle at a collumated source
    particle->position.M[0] = source->pos.M[0];
    particle->position.M[1] = source->pos.M[1] + (rand() % 500 - 250);
    // Set Velocity parrallel to the source
    particle->velocity.M[0] = 1.0l;
    particle->velocity.M[1] = 0.0l;
    particle->velocity = NormalizeMatrix(particle->velocity);
    particle->velocity.M[0] *= particle_speed;
    particle->velocity.M[1] *= particle_speed;
  }
  // Set To White Light
  particle->wave_length = -1;
  particle_color->r = 255;
  particle_color->g = 255;
  particle_color->b = 255;
  particle_color->a = 255;
}

void UpdateParticle(particle_t *particle, double deltaTime) {
  // For each particle

  // Apply velocity
  particle->position.M[0] += particle->velocity.M[0] * deltaTime;
  particle->position.M[1] += particle->velocity.M[1] * deltaTime;
}

void CollideParticleMirror(particle_t *particle, Color *particle_color,
                           matrix_2x1_t normal_matrix) {
  matrix_2x1_t mv = multiply_2x2_by_2x1(calc_plane_mirror_matrix(normal_matrix),
                                        particle->velocity);
  particle->velocity.M[0] = mv.M[0];
  particle->velocity.M[1] = mv.M[1];
}

void CollideParticleWall(particle_t *particle, Color *particle_color,
                         matrix_2x1_t normal_matrix) {
  particle->active = false;
}

void CollideParticleGradient(particle_t *particle, Color *particle_color,
                             matrix_2x1_t normal_matrix) {
  // use the gradient equation to adjust the normal
  matrix_2x1_t normal = normal_matrix;
  float beta = 0;
  int mode = 1;
  // if the particle is white light
  if (particle->wave_length < 0) {
    // assign random wave_length to the particle
    int x = rand() % 4;
    if (x == 0) {
      particle->wave_length = 500 * pow(10, -9);
      *particle_color = BLUE;
    } else if (x == 1) {
      particle->wave_length = 700 * pow(10, -9);
      *particle_color = RED;
    } else if (x == 2) {
      *particle_color = GREEN;
      particle->wave_length = 600 * pow(10, -9);
    } else if (x == 3) {
      *particle_color = VIOLET;
      particle->wave_length = 400 * pow(10, -9);
    }
  }

  // use wave_length to apply gradient equation
  beta = asin(normal.M[1] - mode * particle->wave_length * 60000);
  normal.M[0] = cos(beta);
  normal.M[1] = sin(beta);

  NormalizeMatrix(normal);
  // Apply Mirror Matrix with new normal
  matrix_2x1_t mv =
      multiply_2x2_by_2x1(calc_plane_mirror_matrix(normal), particle->velocity);

  particle->velocity.M[0] = mv.M[0];
  particle->velocity.M[1] = mv.M[1];
}

void DrawParticle(particle_t *particles, Texture2D particle_texture,
                  Color *particle_colors, int i) {
  DrawTexture(particle_texture, particles[i].position.M[0],
              particles[i].position.M[1], particle_colors[i]);
  // DrawCircle(particles[i].position.M[0], particles[i].position.M[1], 2,
  //            particle_colors[i]);
}
