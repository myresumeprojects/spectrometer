#include "./Include/manager.h"
#include "Include/matrix.h"
#include "Include/mirrors.h"
#include "Include/particles.h"
#include "Include/rays.h"
#include "Include/source.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <raylib.h>

#define RAYGUI_IMPLEMENTATION
#include "Include/raygui.h"

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080

/*
void DrawReflectingRay(matrix_2x1_t startPoint, matrix_2x1_t endPoint,
                       mirror_t mirror) {
  matrix_2x1_t collisionPoint = {{0, 0}};
  matrix_2x1_t startToMouse = (matrix_2x1_t){
      {endPoint.M[0] - startPoint.M[0], endPoint.M[1] - startPoint.M[1]}};
  if (CheckCollisionMatrix(startPoint,
                           (matrix_2x1_t){{endPoint.M[0], endPoint.M[1]}},
                           mirror.start, mirror.end, &collisionPoint)) {

    DrawLine(startPoint.M[0], startPoint.M[1], collisionPoint.M[0],
             collisionPoint.M[1], RED);
    matrix_2x1_t transformedMatrix =
        multiply_2x2_by_2x1(calc_mirror_matrix(mirror.normal), startToMouse);
    transformedMatrix = NormalizeMatrix(transformedMatrix);
    DrawLine(
        collisionPoint.M[0], collisionPoint.M[1],
        collisionPoint.M[0] + transformedMatrix.M[0] * RAY_COLLISION_DISTANCE,
        collisionPoint.M[1] + transformedMatrix.M[1] * RAY_COLLISION_DISTANCE,
        RED);
  } else {
    DrawLine((int)startPoint.M[0], (int)startPoint.M[1], (int)endPoint.M[0],
             (int)endPoint.M[1], RED);
  }
}
*/

int main(void) {
  srand(time(NULL));
  double frameStart = 0;
  double frameEnd = 0;
  double deltaTime = 0; // Delta Time
  SetTargetFPS(30);

  float camera_zoom_min = 0.125f;
  float camera_zoom_max = 64.0f;

  InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Spectrometer");
  Vector2 mousePos = GetMousePosition();
  manager m;
  InitManager(&m);

  printf("Succesfully Initialized Manager \n");

  Image particle_image = GenImageColor(5, 5, (Color){0, 0, 0, 1});
  ImageDrawCircle(&particle_image, 2, 2, 2, WHITE);
  Texture2D particle_texture = LoadTextureFromImage(particle_image);

  printf("Succesfully Initialized Texture \n");
  UnloadImage(particle_image);

  bool enable_substeps = true;

  // GUI Variables
  bool radiusEditMode = false;
  bool angleEditMode = false;
  bool sizeEditMode = false;
  char vbTextValue[32] = {0};
  int ui_text_x = 0;
  int ui_text_y = 0;
  Rectangle radiusRect = (Rectangle){ui_text_x, ui_text_y, 100, 30};
  Rectangle angleRect = (Rectangle){ui_text_x, ui_text_y, 100, 30};
  Rectangle sizeRect = (Rectangle){ui_text_x, ui_text_y, 100, 30};
  while (!WindowShouldClose()) {
    frameStart = GetTime();

    mousePos = GetScreenToWorld2D(GetMousePosition(), m.camera);
    // If Space is held, then "Fast Forward" 5x speed
    // else go at normal speed
    if (IsKeyDown(KEY_SPACE)) {
      double num_divisions = 10.0f;
      for (int i = 0; i < num_divisions; i++) {
        UpdateManager(&m, deltaTime / 2.0f, mousePos);
      }
    } else {
      if (enable_substeps) {
        for (int i = 0; i < 2; i++) {
          UpdateManager(&m, deltaTime / 2.0f, mousePos);
        }
      } else {
        UpdateManager(&m, deltaTime, mousePos);
      }
    }

    // If R is pressed Re initialize all particles and rays

    if (IsKeyDown(KEY_R)) {
      if (m.use_particles) {
        for (int i = 0; i < m.particle_num; i++) {
          InitParticle(m.particles + i, m.particle_colors + i,
                       m.sources + rand() % m.source_num);
        }
      }
      if (m.use_rays) {
        m.GENERATE_RAYS = true;
      }
    }

    // Move Camera
    float camera_speed = 500.0f * deltaTime;

    if (m.selectedPlaneIdx == -1 && m.selectedSourceIdx == -1 &&
        m.selectedConcaveMirrorIdx == -1) {
      if (IsKeyDown(KEY_RIGHT)) {
        m.camera.target.x = m.camera.target.x + camera_speed;
      }
      if (IsKeyDown(KEY_LEFT)) {
        m.camera.target.x = m.camera.target.x - camera_speed;
      }
      if (IsKeyDown(KEY_UP)) {
        m.camera.target.y = m.camera.target.y - camera_speed;
      }
      if (IsKeyDown(KEY_DOWN)) {
        m.camera.target.y = m.camera.target.y + camera_speed;
      }
    }

    // Handle Zooming In and Out
    float wheel = GetMouseWheelMove();

    if (wheel != 0) {
      m.camera.offset = GetMousePosition();
      m.camera.target = mousePos;
      float scaleFactor = 1.0f + (0.25f * fabsf(wheel));
      if (wheel < 0)
        scaleFactor = 1.0f / scaleFactor;

      m.camera.zoom = m.camera.zoom * scaleFactor;
      if (m.camera.zoom < camera_zoom_min) {
        m.camera.zoom = camera_zoom_min;
      } else if (m.camera.zoom > camera_zoom_max) {
        m.camera.zoom = camera_zoom_max;
      }
    }

    m.mousePressed = IsMouseButtonDown(0) && !m.mouseDown;
    m.mouseDown = IsMouseButtonDown(0);
    m.mouseReleased = IsMouseButtonReleased(0);

    // Draw the screen
    BeginDrawing();

    ClearBackground(BLACK);
    BeginMode2D(m.camera);

    DrawRectangleLinesEx(m.kill_box, 10, RED);

    if (m.use_particles) {
      for (int i = 0; i < m.particle_num; i++) {
        DrawParticle(m.particles, particle_texture, m.particle_colors, i);
      }
    }

    if (m.use_rays) {
      for (int i = 0; i < m.ray_num; i++) {
        DrawLightRay(m.rays + i, m.drawAllRays);
      }
    }

    DrawSources(m.sources, m.source_num);

    DrawMirrors(&m.plane_surfaces, m.concave_mirrors, true, true,
                m.plane_mirror_num + m.wall_num + m.gradient_num,
                m.concave_mirror_num);

    DrawSelected(&m, mousePos);

    EndMode2D();

    DrawRectangle(0, 0, SCREEN_WIDTH, 60, BLACK);
    DrawRectangleLinesEx((Rectangle){0, 0, SCREEN_WIDTH, 60}, 5, GRAY);

    DrawFPS(20, 20);

    int gui_start = 150;
    if (GuiCheckBox((Rectangle){gui_start, 20, 20, 20}, "Use Particles",
                    &m.use_particles)) {
    }
    if (m.use_particles) {
      gui_start += 200;
      float percent = (float)m.particle_num;
      GuiSliderBar((Rectangle){gui_start, 20, 100, 20}, "Particles ",
                   TextFormat("%i / %i", m.particle_num, m.particle_max),
                   &percent, 0, m.particle_max);
      m.particle_num = (int)percent;
    }
    gui_start += 300;
    if (GuiCheckBox((Rectangle){gui_start, 20, 20, 20}, "Use Rays",
                    &m.use_rays)) {
    }
    if (m.use_rays) {
      gui_start += 100;
      GuiCheckBox((Rectangle){gui_start, 20, 20, 20}, "Draw All Rays",
                  &m.drawAllRays);
      gui_start += 200;
      float percent = (float)m.ray_num;
      GuiSliderBar((Rectangle){gui_start, 20, 200, 20}, "Ray Num ",
                   TextFormat("%i / %i", m.ray_num, m.ray_max), &percent, 0,
                   m.ray_max);

      if ((int)percent != m.ray_num) {
        m.ray_num = (int)percent;
        m.GENERATE_RAYS = true;
      }
      gui_start += 400;

      percent = m.ray_update_delay;
      GuiSliderBar((Rectangle){gui_start, 20, 100, 20}, "Ray Delay ",
                   TextFormat("%.2f / %.1f", m.ray_update_delay, 0.5f),
                   &percent, 0.01f, 0.5f);
      m.ray_update_delay = percent;
    }

    int collumn_width = 300;
    DrawRectangle(SCREEN_WIDTH - collumn_width, 60, collumn_width,
                  SCREEN_HEIGHT - 60, BLACK);
    DrawRectangleLinesEx((Rectangle){SCREEN_WIDTH - collumn_width, 60,
                                     collumn_width, SCREEN_HEIGHT - 60},
                         5, GRAY);

    if (m.selectedSourceIdx == -1 && m.selectedPlaneIdx == -1 &&
        m.selectedConcaveMirrorIdx == -1) {
      if (GuiButton(
              (Rectangle){SCREEN_WIDTH - collumn_width + 50, 60 + 100, 200, 50},
              "Add Source")) {
        AddSource(m.sources, &m.source_num, &m.source_max,
                  (matrix_2x1_t){{0, 0}}, false);
      }
      if (GuiButton((Rectangle){SCREEN_WIDTH - collumn_width + 50, 2 * 60 + 100,
                                200, 50},
                    "Add Plane Mirror")) {
        AddPlaneMirror(&m.plane_surfaces, (matrix_2x1_t){{0, 0}},
                       (matrix_2x1_t){{0, 200}}, 1.0f);
        InitPlaneSurfaces(&m.plane_surfaces);
      }
      if (GuiButton((Rectangle){SCREEN_WIDTH - collumn_width + 50, 3 * 60 + 100,
                                200, 50},

                    "Add Wall")) {
        AddWall(&m.plane_surfaces, (matrix_2x1_t){{0, 0}},
                (matrix_2x1_t){{0, 200}}, 1.0f);
        InitPlaneSurfaces(&m.plane_surfaces);
      }
      if (GuiButton((Rectangle){SCREEN_WIDTH - collumn_width + 50, 4 * 60 + 100,
                                200, 50},
                    "Add Gradient")) {
        AddGradient(&m.plane_surfaces, (matrix_2x1_t){{0, 0}},
                    (matrix_2x1_t){{0, 200}}, 1.0f);
        InitPlaneSurfaces(&m.plane_surfaces);
      }
      if (GuiButton((Rectangle){SCREEN_WIDTH - collumn_width + 50, 5 * 60 + 100,
                                200, 50},
                    "Add Concave Mirror")) {
        AddConcaveMirrors(m.concave_mirrors, &m.concave_mirror_num,
                          &m.concave_mirror_max, (matrix_2x1_t){0, 0}, 1000,
                          1 / 32.0f, -1 / 16.0f);
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }
    }
    if (m.selectedSourceIdx != -1) {
      if (IsKeyDown(KEY_RIGHT)) {
        m.sources[m.selectedSourceIdx].pos.M[0] += 1;
      }
      if (IsKeyDown(KEY_LEFT)) {
        m.sources[m.selectedSourceIdx].pos.M[0] -= 1;
      }
      if (IsKeyDown(KEY_UP)) {
        m.sources[m.selectedSourceIdx].pos.M[1] -= 1;
      }
      if (IsKeyDown(KEY_DOWN)) {
        m.sources[m.selectedSourceIdx].pos.M[1] += 1;
      }

      ui_text_x = SCREEN_WIDTH - collumn_width + 10;
      ui_text_y = 80;
      Color font_color = RAYWHITE;
      int uiFontSize = 20;
      DrawText("Source", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText(TextFormat("X : %.0f, Y : %.0f",
                          m.sources[m.selectedSourceIdx].pos.M[0],
                          m.sources[m.selectedSourceIdx].pos.M[1]),
               ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText(TextFormat("X : %.0f, Y : %.0f", mousePos.x, mousePos.y),
               ui_text_x, ui_text_y, uiFontSize, font_color);

      if (GuiButton((Rectangle){ui_text_x, SCREEN_HEIGHT - 300, 100, 50},
                    "Remove Source")) {
        for (int i = m.selectedSourceIdx; i < m.source_num; i++) {
          m.sources[i].pos.M[0] = m.sources[i + 1].pos.M[0];
          m.sources[i].pos.M[1] = m.sources[i + 1].pos.M[1];
          m.sources[i].point_source = m.sources[i + 1].point_source;
        }
        m.source_num--;
        m.selectedSourceIdx = -1;
      }
    }
    if (m.selectedPlaneIdx != -1) {
      if (IsKeyDown(KEY_RIGHT)) {
        if (m.selectedPlaneStartIdx != -1) {
          m.plane_surfaces.start[m.selectedPlaneIdx].M[0] += 1;
        } else if (m.selectedPlaneEndIdx != -1) {
          m.plane_surfaces.end[m.selectedPlaneIdx].M[0] += 1;
        }
        InitPlaneSurfaces(&m.plane_surfaces);
      }
      if (IsKeyDown(KEY_LEFT)) {
        if (m.selectedPlaneStartIdx != -1) {
          m.plane_surfaces.start[m.selectedPlaneIdx].M[0] -= 1;
        } else if (m.selectedPlaneEndIdx != -1) {
          m.plane_surfaces.end[m.selectedPlaneIdx].M[0] -= 1;
        }
        InitPlaneSurfaces(&m.plane_surfaces);
      }
      if (IsKeyDown(KEY_UP)) {
        if (m.selectedPlaneStartIdx != -1) {
          m.plane_surfaces.start[m.selectedPlaneIdx].M[1] -= 1;
        } else if (m.selectedPlaneEndIdx != -1) {
          m.plane_surfaces.end[m.selectedPlaneIdx].M[1] -= 1;
        }
        InitPlaneSurfaces(&m.plane_surfaces);
      }
      if (IsKeyDown(KEY_DOWN)) {
        if (m.selectedPlaneStartIdx != -1) {
          m.plane_surfaces.start[m.selectedPlaneIdx].M[1] += 1;
        } else if (m.selectedPlaneEndIdx != -1) {
          m.plane_surfaces.end[m.selectedPlaneIdx].M[1] += 1;
        }
        InitPlaneSurfaces(&m.plane_surfaces);
      }

      int ui_text_x = SCREEN_WIDTH - collumn_width + 10;
      int ui_text_y = 80;
      Color font_color = RAYWHITE;
      int uiFontSize = 20;
      switch (m.plane_surfaces.surface_type[m.selectedPlaneIdx]) {
      case MIRROR:
        DrawText("Mirror", ui_text_x, ui_text_y, uiFontSize, font_color);
        break;
      case WALL:
        DrawText("Wall", ui_text_x, ui_text_y, uiFontSize, font_color);
        break;
      case GRADIENT:
        DrawText("Gradient", ui_text_x, ui_text_y, uiFontSize, font_color);
        break;
      default:
        break;
      }
      ui_text_y += 30;
      DrawText("Start", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText(TextFormat("X : %.0f, Y : %.0f",
                          m.plane_surfaces.start[m.selectedPlaneIdx].M[0],
                          m.plane_surfaces.start[m.selectedPlaneIdx].M[1]),
               ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText("End", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText(TextFormat("X : %.0f, Y : %.0f",
                          m.plane_surfaces.end[m.selectedPlaneIdx].M[0],
                          m.plane_surfaces.end[m.selectedPlaneIdx].M[1]),
               ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;

      DrawText(TextFormat("Reflectances : %.2f",
                          m.plane_surfaces.reflectance[m.selectedPlaneIdx]),
               ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      float percent = 0;
      percent = m.plane_surfaces.reflectance[m.selectedPlaneIdx];
      GuiSliderBar(
          (Rectangle){ui_text_x, ui_text_y, 100, 30}, "",
          TextFormat("%.3f", m.plane_surfaces.reflectance[m.selectedPlaneIdx]),
          &percent, 0, 1);
      if (m.plane_surfaces.reflectance[m.selectedPlaneIdx] != percent) {
        m.plane_surfaces.reflectance[m.selectedPlaneIdx] = percent;
        InitPlaneSurfaces(&m.plane_surfaces);
      }
      ui_text_y += 30;

      if (GuiButton((Rectangle){ui_text_x, SCREEN_HEIGHT - 300, 100, 50},
                    "Remove Surface")) {
        for (int i = m.selectedPlaneIdx; i < m.plane_surfaces.surface_num;
             i++) {
          m.plane_surfaces.start[i].M[0] = m.plane_surfaces.start[i + 1].M[0];
          m.plane_surfaces.start[i].M[1] = m.plane_surfaces.start[i + 1].M[1];
          m.plane_surfaces.end[i].M[0] = m.plane_surfaces.end[i + 1].M[0];
          m.plane_surfaces.end[i].M[1] = m.plane_surfaces.end[i + 1].M[1];
          m.plane_surfaces.normal[i].M[0] = m.plane_surfaces.normal[i + 1].M[0];
          m.plane_surfaces.normal[i].M[1] = m.plane_surfaces.normal[i + 1].M[1];
          m.plane_surfaces.surface_type[i] =
              m.plane_surfaces.surface_type[i + 1];
          m.plane_surfaces.reflectance[i] = m.plane_surfaces.reflectance[i + 1];
        }
        m.plane_surfaces.surface_num--;
        m.selectedPlaneIdx = -1;
        InitPlaneSurfaces(&m.plane_surfaces);
      }
    }

    if (m.selectedConcaveMirrorIdx != -1) {
      if (IsKeyDown(KEY_RIGHT)) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].pos.M[0] += 1;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }
      if (IsKeyDown(KEY_LEFT)) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].pos.M[0] -= 1;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }
      if (IsKeyDown(KEY_UP)) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].pos.M[1] -= 1;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }
      if (IsKeyDown(KEY_DOWN)) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].pos.M[1] += 1;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }

      int ui_text_x = SCREEN_WIDTH - collumn_width + 10;
      int ui_text_y = 80;
      Color font_color = RAYWHITE;
      int uiFontSize = 20;
      float percent = 0;
      DrawText("Concave Mirror", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText("Position", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText(
          TextFormat("X : %.0f, Y : %.0f",
                     m.concave_mirrors[m.selectedConcaveMirrorIdx].pos.M[0],
                     m.concave_mirrors[m.selectedConcaveMirrorIdx].pos.M[1]),
          ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText("Radius", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      percent = m.concave_mirrors[m.selectedConcaveMirrorIdx].radius;
      /*GuiSliderBar(
          (Rectangle){ui_text_x, ui_text_y, 100, 30}, "",
          TextFormat("%.3f",
                     m.concave_mirrors[m.selectedConcaveMirrorIdx].radius),
          &percent, 0, 10000);*/
      int val = m.concave_mirrors[m.selectedConcaveMirrorIdx].radius;
      radiusRect = (Rectangle){ui_text_x, ui_text_y, 100, 30};
      if (GuiValueBox(radiusRect, "", &val, 1, 10000, radiusEditMode)) {
        radiusEditMode = !radiusEditMode;
      }
      if (m.concave_mirrors[m.selectedConcaveMirrorIdx].radius != val) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].radius = val;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }
      ui_text_y += 30;

      DrawText(TextFormat("%.0f",
                          m.concave_mirrors[m.selectedConcaveMirrorIdx].radius),
               ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText("Angle", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText(TextFormat("%.3f",
                          m.concave_mirrors[m.selectedConcaveMirrorIdx].offset),
               ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;

      percent = m.concave_mirrors[m.selectedConcaveMirrorIdx].offset;
      angleRect = (Rectangle){ui_text_x, ui_text_y, 100, 30};
      if (GuiValueBoxFloat(angleRect, NULL, vbTextValue, &percent,
                           angleEditMode)) {
        angleEditMode = !angleEditMode;
      }
      if (m.concave_mirrors[m.selectedConcaveMirrorIdx].offset != percent) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].offset = percent;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }
      ui_text_y += 30;
      DrawText("Size", ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      DrawText(TextFormat("%.3f",
                          m.concave_mirrors[m.selectedConcaveMirrorIdx].size),
               ui_text_x, ui_text_y, uiFontSize, font_color);
      percent = m.concave_mirrors[m.selectedConcaveMirrorIdx].size;
      GuiSliderBar(
          (Rectangle){ui_text_x, ui_text_y, 100, 30}, "",
          TextFormat("%.3f",
                     m.concave_mirrors[m.selectedConcaveMirrorIdx].size),
          &percent, -1, 2);
      if (m.concave_mirrors[m.selectedConcaveMirrorIdx].size != percent) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].size = percent;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }

      ui_text_y += 30;
      DrawText(
          TextFormat("Reflectances : %.2f",
                     m.concave_mirrors[m.selectedConcaveMirrorIdx].reflectance),
          ui_text_x, ui_text_y, uiFontSize, font_color);
      ui_text_y += 30;
      percent = m.concave_mirrors[m.selectedConcaveMirrorIdx].reflectance;
      GuiSliderBar(
          (Rectangle){ui_text_x, ui_text_y, 100, 30}, "",
          TextFormat("%.3f",
                     m.concave_mirrors[m.selectedConcaveMirrorIdx].reflectance),
          &percent, 0, 1);
      if (m.concave_mirrors[m.selectedConcaveMirrorIdx].reflectance !=
          percent) {
        m.concave_mirrors[m.selectedConcaveMirrorIdx].reflectance = percent;
        InitConcaveMirrors(m.concave_mirrors, m.concave_mirror_num);
      }
      ui_text_y += 30;
    }

    EndDrawing();
    frameEnd = GetTime();
    deltaTime = frameEnd - frameStart;
  }

  UnloadTexture(particle_texture);
  CloseWindow();

  return 0;
}
